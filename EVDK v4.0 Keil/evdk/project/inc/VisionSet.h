/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Hugo
 *
 * Description: Header file for all the vision sets
 *
 ******************************************************************************
  Change History:

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#ifndef _VISIONSET_H_
#define _VISIONSET_H_

#include "operators.h"
#include "pc_interface.h"
#include "D:/SVN/EVDK/EVDK v4.0 Qt/evdk/ThirdParty/USART.h"
#include "common.h"

// ----------------------------------------------------------------------------
// Function prototypes
// ----------------------------------------------------------------------------
void VisionSet1(image_t *pSrc, image_t *pDst);
void VisionSet2(image_t *pSrc, image_t *pDst);
void VisionSet3(image_t *pSrc, image_t *pDst);
void VisionSet4(image_t *pSrc, image_t *pDst);
void VisionSet5(image_t *pSrc, image_t *pDst);
void VisionSet6(image_t *pSrc, image_t *pDst);
void VisionSet7(image_t *pSrc, image_t *pDst);
void VisionSet8(image_t *pSrc, image_t *pDst);
void VisionSet9(image_t *pSrc, image_t *pDst);

#endif // _VISIONSET_H_
