#ifndef COMMON_H
#define COMMON_H

#include "D:/SVN/EVDK/EVDK v4.0 Qt/evdk/ThirdParty/USART.h"
#include "VisionSet.h"
#include "operators.h"
#include "led.h"
#include "programma.h"

oldblobinfo_t *getOldBlobInfo(void);
dangerzone_t *getDangerZone(void);
void initialize(void);
void save(void);
void USART3_IRQHandler(void);
void handleButtonInput(void);
uint8_t getActive(void);
uint8_t isInDangerZone(Vector2 point);


#endif //COMMON_H
