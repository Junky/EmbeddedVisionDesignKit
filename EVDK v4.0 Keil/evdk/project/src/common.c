#include "common.h"

oldblobinfo_t* oldBlobInfo;
dangerzone_t dangerzone;
uint8_t active = 0;
uint8_t init = 0;
char received_string[MAX_STRLEN+1];

void initialize(void)
{
	if(!init)
	{
		int i;
		oldBlobInfo = (oldblobinfo_t*)malloc(MAXBLOBS * sizeof(oldblobinfo_t));
		init_USART3(BAUDRATE);
		for(i = 0; i < MAXBLOBS; i++)
		{
			oldBlobInfo[i].ID = i;
			oldBlobInfo[i].state = LOST;
			oldBlobInfo[i].lastSeen = 0;
		}
		init = 1;
		USART_puts(USART3, ";50;");
		USART_puts(USART3, ";51;");
	}
}

oldblobinfo_t *getOldBlobInfo(void)
{
	return oldBlobInfo;
}

dangerzone_t *getDangerZone(void)
{
	return &dangerzone;
}

void save(void)
{
	int i;
 	for(i = 0; i < MAXBLOBS; i++)
	{
		if(oldBlobInfo[i].state != LOST && oldBlobInfo[i].state != CALIBRATE)// && oldBlobInfo[i].state != SHOT)
		{
			oldBlobInfo[i].lastSeen++;
			if(oldBlobInfo[i].lastSeen > 5)
			{
				if(oldBlobInfo[i].state == SHOT)
				{
					led_set(3);
				}
				oldBlobInfo[i].state = LOST;
			}
		}
	}
}

//interupt from turret: if got interupt, set 'SHOT' blob to 'GOT'
void USART3_IRQHandler(void)
{
    // check if the USART1 receive interrupt flag was set
    if( USART_GetITStatus(USART3, USART_IT_RXNE))
    {
        static uint8_t cnt = 0; // this counter is used to determine the string length
        char t = USART3->DR; // the character from the USART1 data register is saved in t

        /* check if the received character is not the LF character (used to determine end of string)
         * or the if the maximum string length has been been reached
         */
        if( (t != ';') && (cnt < MAX_STRLEN) )
        {
            received_string[cnt] = t;
            cnt++;
        }
        else
        {
            int i;
					  //USART_puts(USART3, received_string);
            if(received_string[0] == 'd' && received_string[1] == 'o' && received_string[2] == 'n' && received_string[3] == 'e')
						{
								for(i = 0; i < MAXBLOBS; i++)
								{
										if(oldBlobInfo[i].state == SHOT)
										{
					  pc_send_string(received_string);
												oldBlobInfo[i].state = GOT;
										}
								}
						}
            //reset count
            cnt = 0;
        }
    }
}


void handleButtonInput(void)
{
	if(active)
	{
		led_set(1);
		led_set(2);
		led_set(3);
		led_set(4);
	}
	else
	{
		led_reset(1);
		led_reset(2);
		led_reset(3);
		led_reset(4);
	}
	if(programma_get() == 2)
	{
		if(oldBlobInfo[4].state == HOEK)
		{
			led_toggle(1);
		}
		if(oldBlobInfo[5].state == HOEK)
		{
			led_toggle(2);
		}
		if(oldBlobInfo[6].state == HOEK)
		{
			led_toggle(3);
		}
		if(oldBlobInfo[7].state == HOEK)
		{
			led_toggle(4);
		}
		if(oldBlobInfo[4].state == HOEK &&
			 oldBlobInfo[5].state == HOEK &&
			 oldBlobInfo[6].state == HOEK &&
			 oldBlobInfo[7].state == HOEK)
		{
			active = 1;
			dangerzone.leftTop.x = oldBlobInfo[4].Center.x;
			dangerzone.leftTop.y = oldBlobInfo[4].Center.y;
			dangerzone.rightTop.x = oldBlobInfo[5].Center.x;
			dangerzone.rightTop.y = oldBlobInfo[5].Center.y;
			dangerzone.rightBot.x = oldBlobInfo[6].Center.x;
			dangerzone.rightBot.y = oldBlobInfo[6].Center.y;
			dangerzone.leftBot.x = oldBlobInfo[7].Center.x;
			dangerzone.leftBot.y = oldBlobInfo[7].Center.y;
		}
		else if(oldBlobInfo[0].state == REMOVE)
		{
			active = 0;
		}
	}
}

uint8_t getActive(void)
{
	return active;
}


uint8_t isInDangerZone(Vector2 point)
{
	return 1;
}
