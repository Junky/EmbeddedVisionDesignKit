#include "VisionSet.h"
#include "led.h"
#include "programma.h"
#include "D:/SVN/EVDK/EVDK v4.0 Qt/evdk/ThirdParty/USART.h"

// ----------------------------------------------------------------------------
// This vision set is executed when the system powers on
// ----------------------------------------------------------------------------

#include <stdlib.h>

void VisionSet1(image_t *pSrc, image_t *pDst)
{
	oldblobinfo_t *oldBlobInfo = getOldBlobInfo();
	int blobCount;
  int i;
	int amountCalibrated = 1;
  blobinfo_t blobInfo[MAXBLOBS];
	char pSendString[10];
	
	led_set(1);
	initialize();
	
	//send base image to pc
  pc_send_image(pSrc);

	//threshold the image
	//vThresholdIsoData(pSrc, pDst, DARK);
  vThreshold(pSrc, pDst, 0, 55);
	pDst->lut = LUT_BINARY;
	
	//remove borderblobs
	vRemoveBorderBlobs(pDst, pDst, EIGHT);
	
	//fill holes in the blobs
	vFillHoles(pDst, pDst, EIGHT);

	//count the blobs
  blobCount = iLabelBlobs(pDst, pDst, EIGHT);

	//analyse the blobs
	vBlobAnalyse(pDst, blobCount, blobInfo);
	
	
	//filter the blobs depending on size, width and height
	blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 50, FSIZE);
	blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 14, FHEIGHT);
	blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 14, FWIDTH);

	//get blob form of blobs
	vGetBlobForm(blobInfo, blobCount);
	
	//loop though blobs
	for(i = 0; i < blobCount; i++)
	{
		//only if blob form is square
		if(blobInfo[i].form == SQUARE)
		{
			//rotate base image so that theblob is set straight
			vSetStraight(pSrc, pDst, &blobInfo[i]);
			
			//threshold the image
			//vThresholdIsoData(pDst, pDst, DARK);
      vThreshold(pDst, pDst, 0, 55);
	    pc_send_image(pDst);
			
			//set blob ID to 99 to identify the current blob
			blobInfo[i].ID = MAXBLOBS + 1;
			
			//select the current blob by setting all pixels with value 1 to 99(blob ID) that are whithin the original space of the blob
			vSetSelectedBlob(pDst, pDst, 1, &blobInfo[i]);
			
			//get new blob info of the selected blob
      vGetBlobInfo(pDst, blobInfo[i].ID, &blobInfo[i]);
			
			//draw the blob
			vDrawBlob(pDst, blobInfo[i], 2);
			
			//get the aruco ID
			blobInfo[i].ID = iAruco(pDst, blobInfo[i]);
			sprintf(pSendString, "ID: %i\n", blobInfo[i].ID); 
			pc_send_string(pSendString);
		}
	}
	//check the blobs to avoid reading beyond possible data
	if(blobCount > MAXBLOBS)
	{
		blobCount = MAXBLOBS;
	}
	vCopy(pSrc, pDst);
	//loop though all blobs
	for(i = 0; i < blobCount; i++)
	{
			vDrawBlob(pDst, blobInfo[i], 2);
			//all blobs that are squares and have an arudo ID (if aruco ID failes because blob is not aruco, the value will be -1
			if(blobInfo[i].ID != -1 && blobInfo[i].form == SQUARE && blobInfo[i].ID < MAXBLOBS)
			{
					//check for old blobs, set state if position is within moving distance
					oldBlobInfo[blobInfo[i].ID].lastSeen = 0;
				  oldBlobInfo[blobInfo[i].ID].Center = blobInfo[i].Center;
				  oldBlobInfo[blobInfo[i].ID].ID = blobInfo[i].ID;
					if(oldBlobInfo[blobInfo[i].ID].state == LOST)
					{
						oldBlobInfo[blobInfo[i].ID].state = MARKED;
					}
			}
	}

	for(i = 0; i < MAXBLOBS; i++)
	{
			if(oldBlobInfo[i].state == CALIBRATE)
			{
				amountCalibrated++;
			}
	}
	
	
	sprintf(pSendString, "amount: %i\n", amountCalibrated);
  pc_send_string(pSendString);
	if(amountCalibrated > 1)
	{
		led_set(3);
	}
	
	if(oldBlobInfo[amountCalibrated].state == MARKED)
	{
		  //set state to shot
			oldBlobInfo[amountCalibrated].state = CALIBRATE;
			//send data to arduino
			sprintf(pSendString, ";%i,%i,%i;", 51 + amountCalibrated, (int)oldBlobInfo[amountCalibrated].Center.y, (int)oldBlobInfo[amountCalibrated].Center.x);
			USART_puts(USART3, pSendString);
		  pc_send_string(pSendString);
			if(amountCalibrated == 2)
			{
				oldBlobInfo[1].state = GOT;
				oldBlobInfo[2].state = GOT;
				programma_set(2);
			}
	}
	pc_send_image(pDst);
	save();
}
