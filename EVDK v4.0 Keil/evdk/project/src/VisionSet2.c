#include "VisionSet.h"
#include "led.h"
#include <stdlib.h>

void VisionSet2(image_t *pSrc, image_t *pDst)
{
	oldblobinfo_t *oldBlobInfo = getOldBlobInfo();
	int blobCount;
  int i;
	int foundShotBlob = 0;
	int firstMarkedBlob = -1;
	char pString[10];
  blobinfo_t blobInfo[MAXBLOBS];
	//send base image to pc
  pc_send_image(pSrc);

	//threshold the image
	//vThresholdIsoData(pSrc, pDst, DARK);
  vThreshold(pSrc, pDst, 0, 55);
	
	//remove borderblobs
	vRemoveBorderBlobs(pDst, pDst, EIGHT);
	
	//fill holes in the blobs
	vFillHoles(pDst, pDst, EIGHT);
	pDst->lut = LUT_BINARY;

	//count the blobs
  blobCount = iLabelBlobs(pDst, pDst, EIGHT);

	//analyse the blobs
	vBlobAnalyse(pDst, blobCount, blobInfo);
	
	
	//filter the blobs depending on size, width and height
	blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 50, FSIZE);
	blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 14, FHEIGHT);
	blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 14, FWIDTH);

	//get blob form of blobs
	vGetBlobForm(blobInfo, blobCount);
	
	//loop though blobs
	for(i = 0; i < blobCount; i++)
	{
		pc_send_string("marker");
		vDrawBlob(pDst, blobInfo[i], 2);
		//only if blob form is square
		if(blobInfo[i].form == SQUARE)
		{
			//rotate base image so that theblob is set straight
			vSetStraight(pSrc, pDst, &blobInfo[i]);
			
			//threshold the image
			//vThresholdIsoData(pDst, pDst, DARK);
      vThreshold(pDst, pDst, 0, 55);
	pc_send_image(pDst);
			
			
			//set blob ID to 99 to identify the current blob
			blobInfo[i].ID = 99;
			
			//select the current blob by setting all pixels with value 1 to 99(blob ID) that are whithin the original space of the blob
			vSetSelectedBlob(pDst, pDst, 1, &blobInfo[i]);
			
			//get new blob info of the selected blob
      vGetBlobInfo(pDst, blobInfo[i].ID, &blobInfo[i]);
			
			//draw the blob
			vDrawBlob(pDst, blobInfo[i], 2);
			
			//get the aruco ID
			blobInfo[i].ID = iAruco(pDst, blobInfo[i]);
		}
	}
	//check the blobs to avoid reading beyond possible data
	if(blobCount > MAXBLOBS)
	{
		blobCount = MAXBLOBS;
	}
	vCopy(pSrc, pDst);
	//loop though all blobs
	for(i = 0; i < blobCount; i++)
	{
		vDrawBlob(pDst, blobInfo[i], 2);
		if(blobInfo[i].ID == 8)
		{
			blobInfo[i].ID = 0;
		}
		//all blobs that are squares and have an arudo ID (if aruco ID failes because blob is not aruco, the value will be -1
		if(blobInfo[i].ID != -1 && blobInfo[i].form == SQUARE && blobInfo[i].ID < MAXBLOBS)
		{
			//check for old blobs, set state if position is within moving distance
			oldBlobInfo[blobInfo[i].ID].lastSeen = 0;
			oldBlobInfo[blobInfo[i].ID].Center = blobInfo[i].Center;
			oldBlobInfo[blobInfo[i].ID].ID = blobInfo[i].ID;
			if(oldBlobInfo[blobInfo[i].ID].state == LOST)
			{
				oldBlobInfo[blobInfo[i].ID].state = MARKED;
				if(oldBlobInfo[blobInfo[i].ID].ID == 4 || oldBlobInfo[blobInfo[i].ID].ID == 5 || oldBlobInfo[blobInfo[i].ID].ID == 6 || oldBlobInfo[blobInfo[i].ID].ID == 7)
				{
					oldBlobInfo[blobInfo[i].ID].state = HOEK;
				}
				if(oldBlobInfo[blobInfo[i].ID].ID == 0)
				{
					oldBlobInfo[blobInfo[i].ID].state = REMOVE;
				}
			}
		}
	}
	sprintf(pString, "Active: %i", getActive());
	pc_send_string(pString);
	for(i = 0; i < MAXBLOBS; i++)
	{
		sprintf(pString, "%i: %i", i, oldBlobInfo[i].state);
		pc_send_string(pString);
		//check for blob that has been shot
		if(foundShotBlob == 0 && oldBlobInfo[i].state == SHOT)
		{
			foundShotBlob = 1;
		}
		//get first marked blob (if no blob has been shot, this one will be shot
		else if(oldBlobInfo[i].state == MARKED && firstMarkedBlob == -1)
		{
			firstMarkedBlob = i;
		}
	}
	pc_send_image(pDst);
	//if no blob has been shot, shoot the first marked blob
	if(foundShotBlob == 0 && firstMarkedBlob != -1)
	{
		if((getActive() && isInDangerZone(oldBlobInfo[firstMarkedBlob].Center)) || !getActive())
		{
			//set state to shot
			oldBlobInfo[firstMarkedBlob].state = SHOT;
			//send data to arduino
			sprintf(pString, ";%i,%i,%i;", (int)oldBlobInfo[firstMarkedBlob].ID, (int)oldBlobInfo[firstMarkedBlob].Center.y, (int)oldBlobInfo[firstMarkedBlob].Center.x);
			USART_puts(USART3, pString);
			pc_send_string(pString);
		}
		else
		{
			oldBlobInfo[firstMarkedBlob].state = GOT;
		}
		
	}
	handleButtonInput();
	save();
}
