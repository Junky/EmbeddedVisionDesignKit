#ifndef USART_H
#define USART_H

#ifndef QDEBUG_ENABLE
    #include <misc.h>
    #include <stm32f4xx.h>
    #include <stm32f4xx_rcc.h>
    #include <stm32f4xx_gpio.h>
    #include <stm32f4xx_usart.h>
    #include "GPIO.h"

#define MAX_STRLEN 5
#define BAUDRATE 9600

void init_USART3(uint32_t baudrate);
void init_USART2(uint32_t baudrate);
void USART_puts(USART_TypeDef* USARTx, volatile char *s);

#endif
#endif //USART_H
