#include "GPIO.h"
#ifndef QDEBUG_ENABLE
//generic method for init of GPIO with Alternate Function
void init_GPIO_AF(GPIO_TypeDef *GPIOx, uint32_t Periph, uint32_t pins, uint8_t AF_Function, uint16_t *PinSources, uint8_t amount, GPIOOType_TypeDef OType)
{
	uint32_t amount_counter;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(Periph, ENABLE);
	
	/* GPIOB Configuration:  TIM3 CH3 (PB0) and TIM3 CH4 (PB1) */
	GPIO_InitStructure.GPIO_Pin = pins;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = OType;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOx, &GPIO_InitStructure);

	for(amount_counter = 0; amount_counter < amount; amount_counter++)
	{
		GPIO_PinAFConfig(GPIOx, PinSources[amount_counter], AF_Function);
	}
}

//generic method for init of GPIO
void init_GPIO(GPIO_TypeDef *GPIOx, uint32_t Periph, uint32_t pins, GPIOMode_TypeDef Mode, GPIOOType_TypeDef OType)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(Periph, ENABLE);

	GPIO_InitStructure.GPIO_Pin = pins;
	GPIO_InitStructure.GPIO_Mode = Mode;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = OType;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOx, &GPIO_InitStructure);
}

//generic function to set a specific GPIO pin
void set_GPIO_Pin(GPIO_TypeDef *GPIOx, uint16_t pin, uint8_t state)
{
	if(state)
	{
		GPIO_SetBits(GPIOx, pin);
	}
	else
	{
		GPIO_ResetBits(GPIOx, pin);
	}
}
#endif
