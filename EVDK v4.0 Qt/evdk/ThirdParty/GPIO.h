#ifndef GPIO_H
#define GPIO_H


#ifndef QDEBUG_ENABLE
#include <misc.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_usart.h>

void init_GPIO_AF(GPIO_TypeDef *GPIOx, uint32_t Periph, uint32_t pins, uint8_t AF_Function, uint16_t *PinSources, uint8_t amount, GPIOOType_TypeDef OType);
void init_GPIO(GPIO_TypeDef *GPIOx, uint32_t Periph, uint32_t pins, GPIOMode_TypeDef Mode, GPIOOType_TypeDef OType);
void set_GPIO_Pin(GPIO_TypeDef *GPIOx, uint16_t pin, uint8_t state);
#endif
#endif //GPIO_H
