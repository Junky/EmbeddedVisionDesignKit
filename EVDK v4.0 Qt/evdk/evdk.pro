#******************************************************************************
# Project    : Embedded Vision Design
# Copyright  : 2012 HAN Embedded Systems Engineering
# Author     : Hugo
#
# Description: qmake project file
#
#*****************************************************************************
# Change History:
#
#  Version 2.0 - October 2012
#  > New folder structure
#
#  Version 1.0 - September 2011
#  > Initial revision
#  > Project created by QtCreator 2011-10-16T10:41:40
#
#*****************************************************************************/

#QT      +=  core gui
QT      +=  widgets

# Enable this configuration for a debug console
CONFIG  += console

DEFINES += QDEBUG_ENABLE

TARGET   =  evdk
TEMPLATE =  app

INCLUDEPATH += ./ft245app          \
               ./main              \
               ./user              \
               ./ico               \
               ./ThirdParty

RESOURCES +=  ico/icons.qrc

SOURCES +=  main/main.cpp          \
            main/mainwindow.cpp    \
            main/aboutevdk.cpp     \
            main/qpassonevents.cpp \
            user/VisionSet1.cpp    \
            user/VisionSet2.cpp    \
            user/VisionSet3.cpp    \
            user/VisionSet4.cpp    \
            user/VisionSet5.cpp    \
            user/VisionSet6.cpp    \
            user/VisionSet7.cpp    \
            user/VisionSet8.cpp    \
            user/VisionSet9.cpp    \
            ft245app/ft245app.cpp  \
            ft245app/d2xx.cpp      \
            user/operators.cpp \
    ThirdParty/GPIO.c \
    ThirdParty/USART.c

HEADERS +=  main/mainwindow.h      \
            main/aboutevdk.h       \
            main/qpassonevents.h   \
            user/operators.h       \
            user/VisionSet.h       \
            ft245app/ft245app.h    \
            ft245app/ftd2xx.h      \
            ft245app/d2xx.h \
    ThirdParty/GPIO.h \
    ThirdParty/USART.h

FORMS   +=  main/mainwindow.ui     \
            ft245app/ft245app.ui

# Set the libraries that need to be copied and the destination path
ftd2xx_libs.files += "D:/SVN/EVDK/EVDK v4.0 Qt/evdk/WIN32/ftd2xx.dll" \
                     "D:/SVN/EVDK/EVDK v4.0 Qt/evdk/WIN32/ftd2xx.lib"
ftd2xx_libs.path   = $$OUT_PWD/debug

# Install additional files, 'make install' needs to be added as a custom build step
INSTALLS += ftd2xx_libs

win32: LIBS += "D:/SVN/EVDK/EVDK v4.0 Qt/evdk/WIN32/ftd2xx.lib"


