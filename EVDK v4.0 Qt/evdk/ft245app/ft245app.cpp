/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Sander/Hugo
 *
 * Description: Implementation file for the FT245 App
 *
 ******************************************************************************
  Change History:

    Version 2.0 - October 2012
    > Solved bug when closing the FT245 app
    > Added bench duration calculation in seconds

    Version 1.0 - December 2011
    > Initial revision

******************************************************************************/
#include <stdio.h>
#include "ft245app.h"
#include "ui_ft245app.h"

// ----------------------------------------------------------------------------
// Function implementations
// ----------------------------------------------------------------------------
ft245app::ft245app(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ft245app)
{
    ui->setupUi(this);
    ui->bnt_connect_dev->setEnabled(1);
    ui->bnt_disconnect_dev->setEnabled(0);

    connect(&verbinding.object, SIGNAL(NewBenchmark(int,int,QString)),this,SLOT(AddBench(int,int,QString)));
    connect(&verbinding.object, SIGNAL(StatusUpdate(d2xx_status_t)), this, SLOT(StatusUpdate(d2xx_status_t)));
}

ft245app::~ft245app()
{
    if(!ui->bnt_connect_dev->isEnabled() && ui->bnt_disconnect_dev->isEnabled())
        verbinding.CloseDevice();   // MvdB

    delete ui;
}

void ft245app::on_bnt_connect_dev_clicked()
{
    ui->bnt_connect_dev->setEnabled(0);
    verbinding.OpenDevice();
    ui->txt_infobox->clear();
}

void ft245app::on_bnt_disconnect_dev_clicked()
{
    ui->bnt_disconnect_dev->setEnabled(0);
    verbinding.CloseDevice();
}

void ft245app::StatusUpdate(d2xx_status_t status)
{
    if(status == d2xx_connected)
    {
        ui->bnt_connect_dev->setEnabled(0);
        ui->bnt_disconnect_dev->setEnabled(1);
    }
    else
    {
        ui->bnt_connect_dev->setEnabled(1);
        ui->bnt_disconnect_dev->setEnabled(0);
    }
}

void ft245app::closeEvent(QCloseEvent * event)
{
    event->accept();
    verbinding.CloseDevice();
    destroyed(this);
}

void ft245app::AddBench(int begin, int eind, QString str)
{
    QString line;

    line.append(QString(" %1").arg(begin*2, 10, 10));
    line.append(" | ");
    line.append(QString(" %1").arg(eind*2, 10, 10));
    line.append(" | ");
    line.append(QString(" %1").arg(1/((double)(168000000.0/((eind - begin)*2))), 10, 'f', 6));
    line.append(" | ");
    line.append(str);
    if(str.compare("Frame") == 0)
    {
        line.append(QString(" %1 FPS").arg((double)(168000000.0/((eind - begin)*2)), 10, 'f', 6));
    }

    ui->txt_infobox->appendPlainText(line);
}

void ft245app::clearInfo()
{
    ui->txt_infobox->clear();
}
