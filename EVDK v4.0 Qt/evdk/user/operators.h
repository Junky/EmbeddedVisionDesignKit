/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2014 HAN Embedded Systems Engineering
 * Author     : Hugo
 *
 * Description: Header file for the implementation of image operators
 *
 ******************************************************************************
  Change History:

    Version 2.0 - October 2012
    > Implemented new image sructure
    > Added platform dependent QDEBUG macro

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#ifndef _OPERATORS_H_
#define _OPERATORS_H_

#include "stdint.h"

// ----------------------------------------------------------------------------
// Debugging macro
// ----------------------------------------------------------------------------
#ifdef QDEBUG_ENABLE

  #include <QDebug>
  #define QDEBUG(x) qDebug()<<x

  // Example usage:
  // QDEBUG("Debug " << parameter << " value");

#else

  #define QDEBUG(x) //x

#endif

// ----------------------------------------------------------------------------
// Defines
// ----------------------------------------------------------------------------
// Sine utilities
#define DEGREE2RAD 57.295779513082320876798154814105

// Sqrroot utilities
#define SQRROOT2 1.4142135623730950488016887242097
#define SQRROOT5 2.2360679774997896964091736687313

// Labelblob limits
#define MAXBLOBS 8

// blob states
#define LOST 0
#define SHOT 1
#define GOT 2
#define MARKED 3
#define HOEK 4
#define REMOVE 8
#define CALIBRATE 9

// Form factors utilities
#define SQUARE_FACTOR 16
#define TRIANGLE_FACTOR 23.31
#define CIRCLE_FACTOR 12.57

// Image attributes
// Make sure (IMG_HEIGHT * IMG_WIDTH) is a power of 32 (DMA)
#define IMG_HEIGHT  (144)
#define IMG_WIDTH   (176)

// Image displaying LookUp Tables
// These values must match the index values of the Qt dropdownboxes
#define LUT_STRETCH (0)
#define LUT_CLIP    (1)
#define LUT_BINARY  (2)
#define LUT_LABELED (3)

// ----------------------------------------------------------------------------
// Type definitions
// ----------------------------------------------------------------------------
typedef struct
{
  uint16_t width;
  uint16_t height;
  uint16_t lut;
  uint16_t dummy; // Make sure the size of this struct is a power of 32 (DMA)
  uint8_t  data[IMG_HEIGHT][IMG_WIDTH];
}image_t;

typedef struct
{
  float x;
  float y;
}Vector2;

typedef enum
{
  BRIGHT = 0,
  DARK
}eBrightness;

typedef enum
{
  FOUR  = 4,
  EIGHT = 8
}eConnected;

typedef enum
{
  AND = 0,
  OR = 1
}eOperator;

typedef enum
{
  EQUAL = 0,
  HIGHER = 1
}eMethod;

typedef enum
{
  AVERAGE = 0,
  HARMONIC,
  MAX,
  MEDIAN,
  MIDPOINT,
  MIN,
  RANGE
}eFilterOperation;

typedef enum
{
  NONE = 0,
  CIRCLE = 1,
  TRIANGLE = 3,
  SQUARE = 4
}eForm;

typedef enum
{
    FSIZE = 0,
    FWIDTH,
    FHEIGHT
}eFilterType;

typedef struct blobinfo_t
{
  int      ID;
  Vector2  Center;
  uint16_t height;
  uint16_t width;
  uint16_t top;
  uint16_t bottom;
  uint16_t right;
  uint16_t left;
  uint16_t nof_pixels;
  float    perimeter;
  eForm    form;
  Vector2  firstPoint;
  Vector2  leftPoint;
  uint8_t  state;
}blobinfo_t;

typedef struct oldblobinfo_t
{
	int			 ID;
	Vector2  Center;
	uint8_t  state;
	uint8_t  lastSeen;
}oldblobinfo_t;

typedef struct dangerzone_t
{
	Vector2 leftTop;
	Vector2 rightTop;
	Vector2 leftBot;
	Vector2 rightBot;
}dangerzone_t;


// ----------------------------------------------------------------------------
// Function prototypes
// ----------------------------------------------------------------------------

// The contrast of the src image is stretch to 0-255 in the dst image
// This function is required by the Qt PC application. It is used to display
// the source- and destination image. If this function is not implemented, the
// user will see random data.
void vContrastStretch(image_t *src, // must be a greyscale image
                      image_t *dst, // result is a greyscale image
                      uint16_t bottom,
                      uint16_t top);

// All pixels in src image with value between 'low' and 'high' are
// set to 1 in dst image. Rest is set to 0
// This function is used in all VisionSets. Without it, initially nothing will
// seem to happen.
void vThreshold(image_t *src,
                image_t *dst, // result is a binary image
                uint8_t low,
                uint8_t high);

// Rotates an image 180 degrees
// This function is required by the microcontroller application. The camera is
// mounted upside down.
void vRotate180(image_t *img);

// This function does the same as vContrastStretch. However, it always
// stretches from 0 to 255 and implements a fast algorithm.
void vContrastStretchFast(image_t *src,  // must be a greyscale image
                          image_t *dst); // result is a greyscale image

// All pixel values are set to 0
void vErase(image_t *img);

// Src and dst image are the same
void vCopy(image_t *src,
           image_t *dst);

// Src and dst image are added pixel by pixel
// Result in dst image
void vAdd(image_t *src,
          image_t *dst);

// Src picture is binary inverted.
// Result in dst image
void vInvert(image_t *src, // must be a binary image
             image_t *dst);
             
// Src and dst image are multiplied pixel by pixel
// Result in dst image
void vMultiply(image_t *src,
               image_t *dst);

// Src image is copied to dst image
// All pixels in dst image with value 'selected' are set to 'value'
void vSetSelectedToValue(image_t *src,
                         image_t *dst,
                         uint8_t selected,
                         uint8_t value);

// Src image is copied to dst image
// All pixels in dst image with value between 'selectedLow' and 'selectedHigh' (including low and high values) are set to 'value'
void vSetSelectedsToValue(image_t *src,
                          image_t *dst,
                          uint8_t selectedLow,
                          uint8_t selectedHigh,
                          uint8_t value);

// Src image is copied to dst image
// All border pixels of dst image are set to 'value'
void vSetBorders(image_t *src,
                 image_t *dst,
                 uint8_t value);

void vSetBorderSelectedToValue(image_t *src,
                               image_t *dst,
                               uint8_t selected,
                               uint8_t value);

// Uses a bitplane (mask) to slice the source image
void vBitSlicing(image_t *src,
                 image_t *dst,
                 uint8_t mask);

// Make a histogram of the source image and calculate the sum of all pixel
// values
void vHistogram(image_t  *src,
                uint16_t *hist,
                uint32_t *sum);

// Threshold values are automatically generated based on the histogram of
// src image
void vThresholdIsoData(image_t *src,
                       image_t *dst,
                       eBrightness brightness); // DARK | BRIGHT

// Threshold values are automatically generated based on the histogram of
// src image
void vThresholdOtsu(image_t *src,
                    image_t *dst,
                    eBrightness brightness); // DARK | BRIGHT

// Fill holes
void vFillHoles(image_t *src, // must be a binary image
                image_t *dst,
                eConnected connected); // FOUR | EIGHT

// Remove the border blobs
void vRemoveBorderBlobs(image_t *src, // must be a binary image
                        image_t *dst,
                        eConnected connected); // FOUR | EIGHT

// mark border blobs with value
void vBorderMarkBlobs(image_t *src,
                image_t *dst,
                eConnected connected,
                int blobNumber);

// for each pixel with blobNumber, if neighbours are selected (op) set pixel to value
void vSVN(image_t *src,
                image_t *dst,
                eConnected connected, // FOUR | EIGHT
                int blobNumber,
                int selected,
                int value,
                eOperator op, // AND | OR
                eMethod method); // EQUAL | HIGHER

uint8_t iCheckNeighbours(image_t *src,
                      image_t *dst,
                      int x,
                      int y,
                      int selected,
                      int value,
                      eConnected connected,
                      eOperator op,
                      eMethod method);

// Find the edges of binary images
void vBinaryEdgeDetect(image_t *src, // must be a binary image
                       image_t *dst,
                       eConnected connected);

// A set of nonlinear filters
void vNonlinearFilter(image_t *src,
                      image_t *dst,
                      eFilterOperation fo,
                      uint8_t n); // n equals the mask size
                                  // n should be an odd number

// Label all blobs, returns the number of labeled blobs
uint32_t iLabelBlobs(image_t *src, // must be a binary image
                     image_t *dst,
                     eConnected connected);

// Analyse blobs
// pBlobInfo points to a blobinfo_t struct declared by the calling program
void vBlobAnalyse(image_t *img,
                  const uint8_t blobamount,
                  blobinfo_t *pBlobInfo);


void vGetBlobInfo(image_t *img,
                  uint8_t iBlobNr,
                  blobinfo_t *pBlobInfo);


uint32_t iFilterBlobs(image_t *src, image_t *dst, blobinfo_t *pBlobInfo, uint32_t amount, uint32_t filter, eFilterType filterType);

void vGetBlobForm(blobinfo_t *pBlobInfo, uint32_t amount);

float fGetPerimeter(image_t *img, blobinfo_t *pBlobInfo, uint8_t blobNr);

// Calculates the centroid of a blob
void vCentroid(image_t *img, uint8_t blobnr, uint8_t *xc, uint8_t *yc);

int iMoment(image_t *img, uint8_t blobnr, int p, int q);

int iCentralMoment(image_t *img, uint8_t blobnr, int p, int q);

int power(int x, int y);

// Calculates the normalized central moments of a blob specified by p and q
// Ref.: Gonzalez, R. (). 11.3.4 Moment Invariants.
//       In Digital Image Processing. pp. 839-842.
//       New Jersey: Pearson Prentice Hall.
double dNormalizedCentralMoments(image_t *img, // must be a binary or labeled image
                                 uint8_t blobnr, // must be '1' if img is binary
                                 int p,
                                 int q);

// LDF
void vLDF(image_t *src, // must be a binary image
          image_t *dst);

// This function checks the number of pixels around pixel (x,y) and returns the
// number of pixels that equal value.
uint8_t iNeighbourCount(image_t *img,
                        uint16_t x,
                        uint16_t y,
                        uint8_t value,
                        eConnected connected);

uint8_t iNeighboursEqualOrHigher(image_t *img,
                                 uint16_t x,
                                 uint16_t y,
                                 uint8_t value,
                                 eConnected connected);


void vRotatePoint90(image_t *src, image_t *dst, Vector2 rotationPoint);

void vRotatePoint180(image_t *src, image_t *dst, Vector2 rotationPoint);

void vRotateX(image_t *src, image_t *dst, int degrees, Vector2 rotationPoint);

// Aruco
// This function returns the id of the aruco code on the screen. Needed are the aruco code (doesn't matter what rotation, as long as it is square)
// and returns the id coresponding
// will return -1 if the ID cannot be determined
int iAruco(image_t *img,
           blobinfo_t blobInfo);

void vGetAruco(image_t *src, image_t *dst);


void vSetStraight(image_t *src, image_t *dst, blobinfo_t *blobInfo);

void vROI(image_t *src,
          image_t *dst,
          uint16_t x,
          uint16_t y,
          uint16_t width,
          uint16_t height);

void vDrawBlob(image_t *img,
               blobinfo_t blobInfo,
               uint8_t value);

void vSetSelectedBlob(image_t *src, image_t *dst, uint8_t selected, blobinfo_t *blobInfo);

#endif // _OPERATORS_H_
// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
