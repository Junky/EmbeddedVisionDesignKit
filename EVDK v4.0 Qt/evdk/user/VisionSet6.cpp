#include "VisionSet.h"

void VisionSet6(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    vThresholdIsoData(pSrc, pDst, DARK);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("1. vThresholdIsoData DARK\n"));

    vRemoveBorderBlobs(pDst, pDst, EIGHT);
    int blobAmount = iLabelBlobs(pDst, pDst, EIGHT);
    pDst->lut = LUT_LABELED;
    mw->vShowDstImage();
    info.append(QString("2. iLabelBlobs\n"));

    blobinfo_t pBlobInfo[blobAmount];

    vBlobAnalyse(pDst, blobAmount, pBlobInfo);

    for(int i = 0; i < blobAmount; i++)
    {
        double n20 = dNormalizedCentralMoments(pDst, pBlobInfo[i].ID, 2, 0);
        double n02 = dNormalizedCentralMoments(pDst, pBlobInfo[i].ID, 0, 2);
        double n11 = dNormalizedCentralMoments(pDst, pBlobInfo[i].ID, 1, 1);

        double im1 = n20 + n02;
        double im2 = ((n20 - n02) * (n20 - n02)) + (4 * (n11 * n11));
        QDEBUG("n20: " << n20 << ", n02: " << n02 << ", n11: " << n11);

        info.append(QString("3.") + QString::number(pBlobInfo[i].ID) + QString(" \n - IM1: ")+ QString::number(im1) + QString("\n - IM2: ") + QString::number(im2) + QString("\n"));
    }
}
