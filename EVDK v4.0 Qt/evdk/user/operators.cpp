/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2014 HAN Embedded Systems Engineering
 * Author     : Hugo
 *
 * Description: Implementation of image operators
 *              Implement these functions only using C (not C++)!!
 *
 ******************************************************************************
  Change History:

    Version 2.0 - October 2012
    > Implemented new image structure

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#include "operators.h"
#ifdef QDEBUG_ENABLE
#include "math.h"
#else
#include "arm_math.h"
#endif

// ----------------------------------------------------------------------------
// Function implementations
// ----------------------------------------------------------------------------
void vContrastStretch(image_t *src, // must be a greyscale image
                      image_t *dst, // result is a greyscale image
                      uint16_t bottom,
                      uint16_t top)
{
// ****************************************************************************
// Function Variables
// ****************************************************************************
    register uint32_t lPixel = 255;
    register uint32_t hPixel = 0;
    register uint32_t tmpPix;
    register uint32_t i = ((src->height * src->width)/4);
    register float factor = 1;
    uint32_t LUT[256];
    uint32_t *s = (uint32_t *)src->data;
    uint32_t *d = (uint32_t *)dst->data;

    register uint32_t reg;

// ***********************************************************************6*****

    do {
        reg = *s;
        if((reg & 255) < lPixel) {lPixel = (reg & 255);}
        else if((reg & 255) > hPixel) {hPixel = (reg & 255);}
        s++;

        reg = reg >> 8;
        if((reg & 255) < lPixel) {lPixel = (reg & 255);}
        else if((reg & 255) > hPixel) {hPixel = (reg & 255);}
        s++;

        reg = reg >> 8;
        if((reg & 255) < lPixel) {lPixel = (reg & 255);}
        else if((reg & 255) > hPixel) {hPixel = (reg & 255);}
        s++;;

        reg = reg >> 8;
        if((reg & 255) < lPixel) {lPixel = (reg & 255);}
        else if((reg & 255) > hPixel) {hPixel = (reg & 255);}
        s++;
        i-=4;
    } while(i);

    if( (hPixel - lPixel) != 0) {
        factor = (float)(top - bottom) / (float)(hPixel - lPixel);
    }

    tmpPix = lPixel;
    while(tmpPix <= hPixel) {
        LUT[tmpPix] = (uint32_t)(((float)(tmpPix - lPixel) * factor) + 0.5f);
        tmpPix++;
    }

    i = ((src->height * src->width) / 4);
    s = (uint32_t *)src->data;

    do {
        register uint32_t dt = 0;
        dt |= LUT[*s >> 24 & 255];
        dt <<= 8;
        dt |= LUT[*s >> 16 & 255];
        dt <<= 8;
        dt |= LUT[*s >> 8 & 255];
        dt <<= 8;
        dt |= LUT[*s & 255];
        *d = dt;
        d++; s++; i--;
    } while(i);

    return;
// ********************************************

}


// ----------------------------------------------------------------------------
void vThreshold(image_t *src,
                image_t *dst, // result is a binary image
                uint8_t low,
                uint8_t high)
{
    uint8_t *s = (uint8_t *)src->data;
    uint8_t *d = (uint8_t *)dst->data;
    uint32_t i = src->width * src->height;
    do
    {
        if(*s >= low && *s < high)
        {
            *d = 1;
        }
        else
        {
            *d = 0;
        }
        s++;
        d++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vRotate180(image_t *img)
{
#ifdef QDEBUG_ENABLE
    uint8_t *s = (uint8_t *)img->data;
    uint8_t *d = (uint8_t *)&img->data[img->height-1][img->width-1];
    uint8_t swap;
    uint32_t i = img->height * img->width / 2;
    while(i-- > 0)
    {
        swap = *s;
        *s = *d;
        *d = swap;
        s++;
        d--;
    }
#else
    register uint32_t *s = (uint32_t*)img->data;
    register uint32_t *d = (uint32_t*)&img->data[img->height-1][img->width-4];
    register uint32_t ts;
    register uint32_t td;
    register uint32_t i = img->height * img->width / (4*4*2); //vier pixels tegelijk, helft van image gebruiken
    do
    {
        ts = *s;
        td = *d;
        __asm
        {
            REV ts, ts
            REV td, td
        }
        *s = td;
        *d = ts;
        s++;
        d--;
        ts = *s;
        td = *d;
        __asm
        {
            REV ts, ts
            REV td, td
        }
        *s = td;
        *d = ts;
        s++;
        d--;
        ts = *s;
        td = *d;
        __asm
        {
            REV ts, ts
            REV td, td
        }
        *s = td;
        *d = ts;
        s++;
        d--;
        ts = *s;
        td = *d;
        __asm
        {
            REV ts, ts
            REV td, td
        }
        *s = td;
        *d = ts;
        s++;
        d--;
    }
    while(--i > 0);
#endif
}

void vContrastStretchFast(image_t *src, // must be a greyscale image
                          image_t *dst) // result is a greyscale image
{
    vContrastStretch(src, dst, 0, 255);
}

// ----------------------------------------------------------------------------
void vErase(image_t *img)
{
    uint32_t i = (img->width * img->height) / 4;
    uint32_t *s = (uint32_t *)img->data;
    do
    {
        *s = 0;
        s++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vCopy(image_t *src,
           image_t *dst)
{
    int i = (int)((src->width * src->height) / 4);
    uint32_t *s = (uint32_t *)src->data;
    uint32_t *d = (uint32_t *)dst->data;
    do
    {
        *d = *s;
        s++; d++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vAdd(image_t *src,
          image_t *dst)
{
    uint32_t i = (src->width * src->height) / 4;
    uint32_t *s = (uint32_t *)src->data;
    uint32_t *d = (uint32_t *)dst->data;
    do
    {
        *d = *s + *d;
        s++; d++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vInvert(image_t *src, // Must be a binary image
             image_t *dst)
{
    uint32_t i = (src->width * src->height);
    uint8_t *s = (uint8_t *)src->data;
    uint8_t *d = (uint8_t *)dst->data;
    do
    {
        if(*s == 0)
        {
            *d = 1;
        }
        else if(*s == 1)
        {
            *d = 0;
        }
        s++; d++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vMultiply(image_t *src,
               image_t *dst)
{
    uint32_t i = (src->width * src->height);
    uint8_t *s = (uint8_t *)src->data;
    uint8_t *d = (uint8_t *)dst->data;
    do
    {
        *d = *s * *d;
        s++; d++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vSetSelectedToValue(image_t *src,
                         image_t *dst,
                         uint8_t selected,
                         uint8_t value)
{
    uint32_t i = (src->width * src->height);
    uint8_t *s = (uint8_t *)src->data;
    uint8_t *d = (uint8_t *)dst->data;
    do
    {
        if(*s == selected)
        {
            *d = value;
        }
        else
        {
            *d = *s;
        }
        s++; d++;
    }
    while(--i > 0);
}

void vSetSelectedsToValue(image_t *src,
                          image_t *dst,
                          uint8_t selectedLow,
                          uint8_t selectedHigh,
                          uint8_t value)
{
    uint32_t i = (src->width * src->height);
    uint8_t *s = (uint8_t *)src->data;
    uint8_t *d = (uint8_t *)dst->data;
    do
    {
        if(*s >= selectedLow && *s <= selectedHigh)
        {
            *d = value;
        }
        else
        {
            *d = *s;
        }
        s++; d++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vSetBorders(image_t *src,
                 image_t *dst,
                 uint8_t value)
{
    unsigned i;
    if(src != dst)
    {
        vCopy(src, dst);
    }
    for(i = 0; i < src->height; i++)
    {
        dst->data[i][0] = value;
        dst->data[i][src->width-1] = value;
    }
    for(i = 0; i < src->width; i++)
    {
        dst->data[0][i] = value;
        dst->data[src->height-1][i] = value;
    }
}

// ----------------------------------------------------------------------------
void vSetBorderSelectedToValue(image_t *src,
                               image_t *dst,
                               uint8_t selected,
                               uint8_t value)
{
    unsigned i;
    if(src != dst)
    {
        vCopy(src, dst);
    }
    for(i = 0; i < src->height; i++)
    {
        if(dst->data[i][0] == selected)
        {
            dst->data[i][0] = value;
        }
        if(dst->data[i][src->width-1] == selected)
        {
            dst->data[i][src->width-1] = value;
        }

    }
    for(i = 0; i < src->width; i++)
    {
        if(dst->data[0][i] == selected)
        {
            dst->data[0][i] = value;
        }
        if(dst->data[src->height-1][i] == selected)
        {
            dst->data[src->height-1][i] = value;
        }
    }
		i = 0;
}

// ----------------------------------------------------------------------------
void vBitSlicing(image_t *src,
                 image_t *dst,
                 uint8_t mask)
{
    uint32_t i = (src->width * src->height);
    uint8_t *s = (uint8_t *)src->data;
    uint8_t *d = (uint8_t *)dst->data;
    do
    {
        *d = *s & mask;
        s++; d++;
    }
    while(--i > 0);
}

// ----------------------------------------------------------------------------
void vHistogram(image_t  *src,
                uint16_t *hist,
                uint32_t *sum)
{
    uint32_t i = 0;
    uint8_t *s = (uint8_t*)src->data;
    do
    {
        *sum += *s;
        hist[*s]++;
        s++;
        i++;
    }
    while(i < (uint32_t)(src->height * src->width));
}

// ----------------------------------------------------------------------------
void vThresholdIsoData(image_t *src,
                       image_t *dst,
                       eBrightness brightness) // DARK | BRIGHT
{
    uint16_t hist[256] = {0};
    uint32_t sum;
    uint8_t num;
    uint8_t nextNum;
    uint32_t i = 0;
    vHistogram(src, hist, &sum);
    do
    {
        nextNum = i++;
    }
    while(hist[i] == 0);

    do
    {
        uint32_t n1 = 0;
        uint32_t n2 = 0;
        uint32_t j = 0;
        uint32_t n1i = 0;
        uint32_t n2i = 0;
        num = nextNum;
        do
        {
            n1 += hist[j] * j;
            n1i += hist[j];
            j++;
        }
        while(j < nextNum);
        do
        {
            n2 += hist[j] * j;
            n2i += hist[j];
            j++;
        }
        while(j < 255);
        if(n1i != 0 && n2i != 0)
        {
            n1 /= n1i;
            n2 /= n2i;
        }
        j = n1;
        nextNum = (n1 + n2) / 2;
    }
    while(num != nextNum);

    QDEBUG("ThresholdNumber: " << num);

    if(brightness == BRIGHT)
    {
        vThreshold(src, dst, num, 255);
    }
    else
    {
        vThreshold(src, dst, 0, num);
    }
}

void vThresholdOtsu(image_t *src,
                    image_t *dst,
                    eBrightness brightness) // DARK | BRIGHT
{
    uint16_t hist[256] = {0};
    uint32_t sum;
    vHistogram(src, hist, &sum);
    dst = dst;
    brightness = brightness;
}

// ----------------------------------------------------------------------------
void vFillHoles(image_t *src, // must be a binary image
                image_t *dst,
                eConnected connected) // FOUR | EIGHT
{
    if(connected == FOUR)
    {
        vBorderMarkBlobs(src, dst, EIGHT, 0);
    }
    else
    {
        vBorderMarkBlobs(src, dst, FOUR, 0);
    }
    vSetSelectedToValue(dst, dst, 0, 1);
    vSetSelectedToValue(dst, dst, 2, 0);
}

// ----------------------------------------------------------------------------
void vRemoveBorderBlobs(image_t *src, // must be a binary image
                        image_t *dst,
                        eConnected connected) // FOUR | EIGHT
{
    vBorderMarkBlobs(src, dst, connected, 1);
    vSetSelectedToValue(dst, dst, 2, 0);
}

void vBorderMarkBlobs(image_t *src,
                image_t *dst,
                eConnected connected,
                int blobNumber)
{
    vSetBorderSelectedToValue(src, dst, blobNumber, 2);
    vSVN(src, dst, connected, blobNumber, 2, 2, OR, EQUAL);
}

void vSVN(image_t *src,
            image_t *dst,
            eConnected connected,
            int blobNumber,
            int selected,
            int value,
            eOperator op,
            eMethod method)
{
    int i, j;
    int iMax = dst->width;
    int iFound;

    do
    {
        iFound = 0;
        for(i = 1; i < iMax; i++)
        {
            for(j = 1; j < dst->height; j++)
            {
                int x = j;
                int y = i;
                if(dst->data[x][y] == blobNumber)
                {
                    iFound += iCheckNeighbours(src, dst, x, y, selected, value, connected, op, method);
                }
                x = dst->height - j;
                y = dst->width - i;
                if(dst->data[x][y] == blobNumber)
                {
                    iFound += iCheckNeighbours(src, dst, x, y, selected, value, connected, op, method);
                }
            }
        }
        iMax = dst->height;
        for(j = 1; j < iMax; j++)
        {
            for(i = 1; i < dst->width; i++)
            {
                int x = j;
                int y = i;
                if(dst->data[x][y] == blobNumber)
                {
                    iFound += iCheckNeighbours(src, dst, x, y, selected, value, connected, op, method);
                }
                x = dst->height - j;
                y = dst->width - i;
                if(dst->data[x][y] == blobNumber)
                {
                    iFound += iCheckNeighbours(src, dst, x, y, selected, value, connected, op, method);
                }
            }
        }
    }
    while(iFound != 0);
}

uint8_t iCheckNeighbours(image_t *src,
                      image_t *dst,
                      int x,
                      int y,
                      int selected,
                      int value,
                      eConnected connected,
                      eOperator op,
                      eMethod method)
{
    if(op == AND)
    {
        if(method == EQUAL)
        {
            if(iNeighbourCount(src, x, y, selected, connected) == connected)
            {
                dst->data[x][y] = value;
                return 1;
            }
        }
        else if(method == HIGHER)
        {
            if(iNeighboursEqualOrHigher(src, x, y, selected, connected) == connected)
            {
                dst->data[x][y] = value;
                return 1;
            }
        }
    }
    else if(op == OR)
    {
        if(method == EQUAL)
        {
            if(iNeighbourCount(src, x, y, selected, connected) > 0)
            {
                dst->data[x][y] = value;
                return 1;
            }
        }
        else if(method == HIGHER)
        {
            if(iNeighboursEqualOrHigher(src, x, y, selected, connected) > 0)
            {
                dst->data[x][y] = value;
                return 1;
            }
        }
    }
    return 0;
}

// ----------------------------------------------------------------------------
void vBinaryEdgeDetect(image_t *src, // must be a binary image
                       image_t *dst,
                       eConnected connected)
{
    if(connected == EIGHT)
    {
        vSVN(src, dst, FOUR, 1, 1, 2, AND, HIGHER);
    }
    else if(connected == FOUR)
    {
        vSVN(src, dst, EIGHT, 1, 1, 2, AND, HIGHER);
    }
    vSetSelectedToValue(dst, dst, 2, 0);
}

// ----------------------------------------------------------------------------
void vNonlinearFilter(image_t *src,
                      image_t *dst,
                      eFilterOperation fo,
                      uint8_t n) // n equals the mask size
                                 // n should be an odd number
{
    int half = (n - 1) / 2;
    vCopy(src, dst);
    switch(fo)
    {
        case(AVERAGE):
        {
            int i, j, x, y;
            for(x = half; x < src->height - half; x++)
            {
                for(y = half; y < src->width - half; y++)
                {
                    uint32_t value = 0;
                    for(i = 0; i < n; i++)
                    {
                        for(j = 0; j < n; j++)
                        {
                            value += src->data[(x - half) + i][(y - half) + j];
                        }
                    }
                    dst->data[x][y] = value / (n * n);
                }
            }
        }
        break;
        case(HARMONIC):
        {
            int i, j, x, y;
            for(x = half; x < src->height - half; x++)
            {
                for(y = half; y < src->width - half; y++)
                {
                    uint32_t value;
                    float val = 0.0f;
                    uint8_t nul = 0;
                    for(i = 0; i < n; i++)
                    {
                        for(j = 0; j < n; j++)
                        {
                            if(nul == 0)
                            {
                                value = src->data[(x - half) + i][(y - half) + j];
                                if(value == 0)
                                {
                                    nul = 1;
                                }
                                else
                                {
                                    val += 1.0f / (float)value;
                                }
                            }
                        }
                    }
                    if(nul == 1)
                    {
                        dst->data[x][y] = 0;
                    }
                    else
                    {
                        dst->data[x][y] = (uint8_t)((n * n) / val);
                    }
                }
            }
        }
        break;
        case(MAX):
        {
            int i, j, x, y;
            for(x = half; x < src->height - half; x++)
            {
                for(y = half; y < src->width - half; y++)
                {
                    uint32_t value = 0;
                    for(i = 0; i < n; i++)
                    {
                        for(j = 0; j < n; j++)
                        {
                            if(value < src->data[(x - half) + i][(y - half) + j])
                            {
                                value = src->data[(x - half) + i][(y - half) + j];
                            }
                        }
                    }
                    dst->data[x][y] = value;
                }
            }
        }
        break;
        case(MEDIAN):
        {
            int i, j, x, y;
            for(x = half; x < src->height - half; x++)
            {
                for(y = half; y < src->width - half; y++)
                {
                    uint8_t values[256] = { 0 };
                    for(i = 0; i < n; i++)
                    {
                        for(j = 0; j < n; j++)
                        {
                            values[src->data[(x - half) + i][(y - half) + j]]++;
                        }
                    }
                    i = 0;
                    j = -1;
                    while(i <= half && j < 255)
                    {
                        j++;
                        i += values[j];
                    }
                    dst->data[x][y] = j;
                }
            }
        }
        break;
        case(MIDPOINT):
        {
            int i, j, x, y;
            for(x = half; x < src->height - half; x++)
            {
                for(y = half; y < src->width - half; y++)
                {
                    uint32_t max = 0;
                    uint32_t min = 255;
                    for(i = 0; i < n; i++)
                    {
                        for(j = 0; j < n; j++)
                        {
                            if(max < src->data[(x - half) + i][(y - half) + j])
                            {
                                max = src->data[(x - half) + i][(y - half) + j];
                            }
                            else if(min > src->data[(x - half) + i][(y - half) + j])
                            {
                                min = src->data[(x - half) + i][(y - half) + j];
                            }
                        }
                    }
                    dst->data[x][y] = (min + max) / 2;
                }
            }
        }
        break;
        case(MIN):
        {
            int i, j, x, y;
            for(x = half; x < src->height - half; x++)
            {
                for(y = half; y < src->width - half; y++)
                {
                    uint32_t value = 255;
                    for(i = 0; i < n; i++)
                    {
                        for(j = 0; j < n; j++)
                        {
                            if(value > src->data[(x - half) + i][(y - half) + j])
                            {
                                value = src->data[(x - half) + i][(y - half) + j];
                            }
                        }
                    }
                    dst->data[x][y] = value;
                }
            }
        }
        break;
        case(RANGE):
        {
            int i, j, x, y;
            for(x = half; x < src->height - half; x++)
            {
                for(y = half; y < src->width - half; y++)
                {
                    uint32_t max = 0;
                    uint32_t min = 255;
                    for(i = 0; i < n; i++)
                    {
                        for(j = 0; j < n; j++)
                        {
                            if(max < src->data[(x - half) + i][(y - half) + j])
                            {
                                max = src->data[(x - half) + i][(y - half) + j];
                            }
                            else if(min > src->data[(x - half) + i][(y - half) + j])
                            {
                                min = src->data[(x - half) + i][(y - half) + j];
                            }
                        }
                    }
                    dst->data[x][y] = max - min;
                }
            }
        }
        break;
    }
    return;
}

// ----------------------------------------------------------------------------
uint32_t iLabelBlobs(image_t *src, // must be a binary image
                     image_t *dst,
                     eConnected connected)
{
    int blobCounter = 0;
    int i = dst->height * dst->width;
    uint8_t *s = (uint8_t *)dst->data;
    vSetSelectedsToValue(src, dst, 1, 254, 255); //used to be able to relabel a labeled image
    do
    {
        if(*s == 255)
        {
            blobCounter++;
            *s = blobCounter;
            vSVN(dst, dst, connected, 255, blobCounter, blobCounter, OR, EQUAL);
            if(blobCounter >= MAXBLOBS)
            {
                  return blobCounter;
            }
        }
        s++;
    }
    while(i-- > 0);
    return blobCounter;
}

// ----------------------------------------------------------------------------
void vBlobAnalyse(image_t *img,
                  const uint8_t blobcount,
                  blobinfo_t *pBlobInfo)
{
    uint8_t tmp = blobcount;
    if(blobcount == 0)
    {
        return;
    }
    do
    {
        pBlobInfo[tmp - 1].width = 0;
        pBlobInfo[tmp - 1].height = 0;
        vGetBlobInfo(img, tmp, &pBlobInfo[tmp - 1]);
    }
    while(--tmp > 0);
}

void vGetBlobInfo(image_t *img,
                  uint8_t iBlobNr,
                  blobinfo_t *pBlobInfo)
{
    uint16_t nrOfPixels = 0;
    uint16_t top = img->height;
    uint16_t bottom = 0;
    uint16_t left = img->width;
    uint16_t right = 0;
    uint8_t first = 1;
    int i, j;
    pBlobInfo->ID = iBlobNr;
    pBlobInfo->leftPoint.x = img->width;
    for(i = 0; i < img->height; i++)
    {
        for(j = 0; j < img->width; j++)
        {
            if(img->data[i][j] == iBlobNr)
            {
                if(first)
                {
                    first = 0;
                    pBlobInfo->firstPoint.x = j;
                    pBlobInfo->firstPoint.y = i;
                }
                nrOfPixels++;
                if(i > bottom) { bottom = i; }
                if(i < top) { top = i; }
                if(j > right) { right = j; }
                if(j < left) { left = j; }
                if(j < pBlobInfo->leftPoint.x)
                {
                    pBlobInfo->leftPoint.x = j;
                    pBlobInfo->leftPoint.y = i;
                }
            }
        }
    }
    pBlobInfo->nof_pixels = nrOfPixels;
    pBlobInfo->height = (bottom - top) + 1;
    pBlobInfo->width = (right - left) + 1;
    pBlobInfo->top = top;
    pBlobInfo->bottom = bottom;
    pBlobInfo->right = right;
    pBlobInfo->left = left;
    pBlobInfo->Center.y = (float)left + ((float)pBlobInfo->width / 2.0f);
    pBlobInfo->Center.x = (float)top + ((float)pBlobInfo->height / 2.0f);
    pBlobInfo->perimeter = fGetPerimeter(img, pBlobInfo, iBlobNr);
}

uint32_t iFilterBlobs(image_t *src, image_t *dst, blobinfo_t *pBlobInfo, uint32_t amount, uint32_t filter, eFilterType filterType)
{
    unsigned i;
    int blobCount;
    if(src != dst)
    {
        vCopy(src, dst);
    }
    switch(filterType)
    {
        case FSIZE:
        {
            for(i = 0; i < amount; i++)
            {
                if(pBlobInfo[i].nof_pixels < filter)
                {
                    vSetSelectedToValue(dst, dst, pBlobInfo[i].ID, 0);
                }
            }
        }
        break;
        case FWIDTH:
        {
            for(i = 0; i < amount; i++)
            {
                if(pBlobInfo[i].width < filter)
                {
                    vSetSelectedToValue(dst, dst, pBlobInfo[i].ID, 0);
                }
            }
        }
        break;
        case FHEIGHT:
        {
            for(i = 0; i < amount; i++)
            {
                if(pBlobInfo[i].height < filter)
                {
                    vSetSelectedToValue(dst, dst, pBlobInfo[i].ID, 0);
                }
            }
        }
        break;
    }
    blobCount = iLabelBlobs(dst, dst, EIGHT);
    vBlobAnalyse(dst, blobCount, pBlobInfo);
    return blobCount;
}

void vGetBlobForm(blobinfo_t *pBlobInfo, uint32_t amount)
{
    float squareDiff, triangleDiff, circleDiff;
    unsigned i;
    for(i = 0; i < amount; i++)
    {
        float formfactor = (pBlobInfo[i].perimeter * pBlobInfo[i].perimeter) / (float)pBlobInfo[i].nof_pixels;
        squareDiff = (float)SQUARE_FACTOR - formfactor;
        triangleDiff = (float)TRIANGLE_FACTOR - formfactor;
        circleDiff = (float)CIRCLE_FACTOR - formfactor;
        if(squareDiff < 0)
        {
            squareDiff *= -1;
        }
        if(triangleDiff < 0)
        {
            triangleDiff *= -1;
        }
        if(circleDiff < 0)
        {
            circleDiff *= -1;
        }
        pBlobInfo[i].form = NONE;
        if(triangleDiff < squareDiff && triangleDiff < circleDiff && triangleDiff < 4)
        {
            pBlobInfo[i].form = TRIANGLE;
        }
        else if(circleDiff < triangleDiff && circleDiff < squareDiff && circleDiff < 4)
        {
            pBlobInfo[i].form = CIRCLE;
        }
        else if(squareDiff < triangleDiff && squareDiff < circleDiff && squareDiff < 4)
        {
            pBlobInfo[i].form = SQUARE;
        }
    }
}

float fGetPerimeter(image_t *img, blobinfo_t *pBlobInfo, uint8_t blobNr)
{
    int i, j;
    float perimeterCount = 0;
    for(i = pBlobInfo->top; i < pBlobInfo->bottom + 1; i++)
    {
        for(j = pBlobInfo->left; j < pBlobInfo->right + 1; j++)
        {
            if(img->data[i][j] == blobNr)
            {
                int neighbourCount = iNeighbourCount(img, i, j, 0, FOUR);
                if(neighbourCount == 1)
                {
                    perimeterCount += (float)1;
                }
                else if(neighbourCount == 2)
                {
                    perimeterCount += (float)SQRROOT2;
                }
                else if(neighbourCount == 3)
                {
                    perimeterCount += (float)SQRROOT5;
                }
                else if(neighbourCount == 4)
                {
                    perimeterCount += (2.0f * (float)SQRROOT2);
                }
            }
        }
    }
    return perimeterCount;
}

void vCentroid(image_t *img, uint8_t blobnr, uint8_t *xc, uint8_t *yc)
{
    int moment10, moment01, moment00;
    moment00 = iMoment(img, blobnr, 0, 0);
    moment01 = iMoment(img, blobnr, 0, 1);
    moment10 = iMoment(img, blobnr, 1, 0);

    *xc = (uint8_t)(moment10 / moment00);
    *yc = (uint8_t)(moment01 / moment00);
}

int iMoment(image_t *img, uint8_t blobnr, int p, int q)
{
    int i, j;
    int ret = 0;
    for(i = 0; i < img->height; i++)
    {
        for(j = 0; j < img->width; j++)
        {
            if(img->data[i][j] == blobnr)
            {
                if(p == 1)
                {
                    ret += i;
                }
                else if(q == 1)
                {
                    ret += j;
                }
                else
                {
                    ret += 1;
                }
            }
        }
    }
    return ret;
}

int iCentralMoment(image_t *img, uint8_t blobnr, int p, int q)
{
    int i, j;
    int ret = 0;
    int moment00;
    uint8_t xc, yc;
    if((p == 0 && q == 1) || (p == 1 && q == 0))
    {
        return 0.0;
    }
    moment00 = iMoment(img, blobnr, 0, 0);
    if(p == 0 && q == 0)
    {
        return moment00;
    }
    vCentroid(img, blobnr, &xc, &yc);

    for(i = 0; i < img->height; i++)
    {
        for(j = 0; j < img->width; j++)
        {
            if(img->data[i][j] == blobnr)
            {
                ret += power(i - xc, p) * power(j - yc, q);
            }
        }
    }
    return ret;
}

int power(int x, int y)
{
    int i;
    int ret = x;
    if(y <= 0)
    {
        return 1;
    }
    for(i = 1; i < y; i++)
    {
        ret *= x;
    }
    return ret;
}

double dNormalizedCentralMoments(image_t *img, uint8_t blobnr, int p, int q)
{
    int y = ((int)(p + q) / 2) + 1;
    if((p == 0 && q == 1) || (p == 1 && q == 0))
    {
        return 0.0;
    }
    if(p == 0 && q == 0)
    {
        return 1.0;
    }
    return (double)(double)(iCentralMoment(img, blobnr, p, q) / (double)power(iCentralMoment(img, blobnr, 0, 0), y));
}

void vLDF(image_t *src,
          image_t *dst)
{

// ********************************************
// Added to prevent compiler warnings
// Remove these when implementation starts
    src=src;
    dst=dst;

    return;
// ********************************************

}

uint8_t iNeighbourCount(image_t *img,
                        uint16_t x,
                        uint16_t y,
                        uint8_t value,
                        eConnected connected)
{
    uint8_t iAmountFound = 0;
    if(img->data[x - 1][y] == value)
    {
        iAmountFound++;
    }
    if(img->data[x + 1][y] == value)
    {
        iAmountFound++;
    }
    if(img->data[x][y - 1] == value)
    {
        iAmountFound++;
    }
    if(img->data[x][y + 1] == value)
    {
        iAmountFound++;
    }
    if(connected == EIGHT)
    {

        if(img->data[x - 1][y - 1] == value)
        {
            iAmountFound++;
        }
        if(img->data[x - 1][y + 1] == value)
        {
            iAmountFound++;
        }
        if(img->data[x + 1][y - 1] == value)
        {
            iAmountFound++;
        }
        if(img->data[x + 1][y + 1] == value)
        {
            iAmountFound++;
        }
    }
    return iAmountFound;
}

uint8_t iNeighboursEqualOrHigher(image_t *img,
                                 uint16_t x,
                                 uint16_t y,
                                 uint8_t value,
                                 eConnected connected)
{
    int iAmountFound = 0;
    if(img->data[x - 1][y] >= value)
    {
        iAmountFound++;
    }
    if(img->data[x + 1][y] >= value)
    {
        iAmountFound++;
    }
    if(img->data[x][y - 1] >= value)
    {
        iAmountFound++;
    }
    if(img->data[x][y + 1] >= value)
    {
        iAmountFound++;
    }
    if(connected == EIGHT)
    {

        if(img->data[x - 1][y - 1] >= value)
        {
            iAmountFound++;
        }
        if(img->data[x - 1][y + 1] >= value)
        {
            iAmountFound++;
        }
        if(img->data[x + 1][y - 1] >= value)
        {
            iAmountFound++;
        }
        if(img->data[x + 1][y + 1] >= value)
        {
            iAmountFound++;
        }
    }
    return iAmountFound;
}

void vRotateX(image_t *src, image_t *dst, int degrees, Vector2 rotationPoint)
{
    float s;
    float c;
    float offsetX;
    float offsetY;
    int i, j;
    float x, y;
    QDEBUG("X: " << rotationPoint.x << ", Y: " << rotationPoint.y);
    while(degrees >= 360)
    {
        degrees -= 360;
    }
    s = sin((float)degrees / (float)DEGREE2RAD);
    c = cos((float)degrees / (float)DEGREE2RAD);

    vErase(dst);
    vSetSelectedsToValue(dst, dst, 0, 254, 255);

    offsetX = ((float)rotationPoint.x * c - (float)rotationPoint.y * s) - (float)rotationPoint.x;
    offsetY = ((float)rotationPoint.x * s + (float)rotationPoint.y * c) - (float)rotationPoint.y;

    QDEBUG("X: " << offsetX << ", Y:" << offsetY);

    for(i = 0; i < src->height; i++)
    {
        for(j = 0; j < src->width; j++)
        {
            x = ((float)i*c - (float)j*s) - offsetX;
            y = ((float)i*s + (float)j*c) - offsetY;
            if(x > 1 && x < src->height - 2 && y > 1 && y < src->width - 2)
            {
                dst->data[(uint8_t)x][(uint8_t)y] = src->data[i][j];
            }
        }
    }
}


void vGetAruco(image_t *src, image_t *dst)
{
    image_t temp;
    blobinfo_t blobInfo[100];
    int blobCount;
    int i;
    temp.width = src->width;
    temp.height = src->height;
    temp.dummy = src->dummy;
    temp.lut = src->lut;
    vThresholdIsoData(src, dst, DARK);
    vRemoveBorderBlobs(dst, dst, EIGHT);
    vCopy(dst, &temp);
    //vFillHoles(&temp, &temp, EIGHT);
    blobCount = iLabelBlobs(&temp, &temp, EIGHT);
    vBlobAnalyse(&temp, blobCount, blobInfo);
    blobCount = iFilterBlobs(&temp, &temp, blobInfo, blobCount, 50, FSIZE);
    vGetBlobForm(blobInfo, blobCount);
    for(i = 0; i < blobCount; i++)
    {
        if(blobInfo[i].form == SQUARE)
        {
            vDrawBlob(dst, blobInfo[i], 2);
            vSetStraight(dst, &temp, &blobInfo[i]);
            QDEBUG("ID: " << iAruco(&temp, blobInfo[i]));
        }
    }
}

void vSetStraight(image_t *src, image_t *dst, blobinfo_t *blobInfo)
{
    float height;
    float width;
    float degrees;
    if(blobInfo->firstPoint.x == blobInfo->leftPoint.x)
    {
        vCopy(src, dst);
        return;
    }
    height = blobInfo->leftPoint.y - blobInfo->firstPoint.y;
    width = blobInfo->firstPoint.x - blobInfo->leftPoint.x;

    degrees = atan((double)width / (double)height);

    vRotateX(src, dst, (uint16_t)(degrees * (float)DEGREE2RAD), blobInfo->Center);
    //vGetBlobInfo(dst, blobInfo->ID, blobInfo);
}

int iAruco(image_t *img,
           blobinfo_t blobInfo)
{
    float gridSizeX, gridSizeY;
    unsigned w, x, y, z;
	  int tries;
    int number = 0;
    int grid[5][5];
    int possibleGrids[4][5];
    if(blobInfo.width < 7 || blobInfo.height < 7 || blobInfo.nof_pixels < 50)
    {
        return -1;
    }
    possibleGrids[0][0] = 1;
    possibleGrids[0][1] = 0;
    possibleGrids[0][2] = 0;
    possibleGrids[0][3] = 0;
    possibleGrids[0][4] = 0;

    possibleGrids[1][0] = 1;
    possibleGrids[1][1] = 0;
    possibleGrids[1][2] = 1;
    possibleGrids[1][3] = 1;
    possibleGrids[1][4] = 1;

    possibleGrids[2][0] = 0;
    possibleGrids[2][1] = 1;
    possibleGrids[2][2] = 0;
    possibleGrids[2][3] = 0;
    possibleGrids[2][4] = 1;

    possibleGrids[3][0] = 0;
    possibleGrids[3][1] = 1;
    possibleGrids[3][2] = 1;
    possibleGrids[3][3] = 1;
    possibleGrids[3][4] = 0;

    gridSizeX = blobInfo.width / 7.0f;
    gridSizeY = blobInfo.height / 7.0f;
    for(w = 1; w < 6; w++)
    {
        for(x = 1; x < 6; x ++)
        {
            int black = 0;
            int white = 0;
            int yVal = (uint16_t)((float)x * gridSizeX) + blobInfo.left;
            int zVal = (uint16_t)((float)w * gridSizeY) + blobInfo.top;
            for(y = yVal; y < yVal + gridSizeX; y++)
            {
                for(z = zVal; z < zVal + gridSizeY; z++)
                {
                    if(img->data[z][y] == 0)
                    {
                        white++;
                    }
                    else
                    {
                        black++;
                    }
                }
            }
            if(black > white)
            {
                grid[w-1][x-1] = 0;
            }
            else
            {
                grid[w-1][x-1] = 1;
            }
        }
    }
    ///TODO: set in function, check 4 times, rotate image...
    tries = 0;
    for(w = 0; w < 5; w++)
    {
        int line = 0;
        int i = 0;
        if(tries == 0)
        {
            while(i != 4)
            {
                if(grid[w][0] == possibleGrids[i][0] &&
                   grid[w][1] == possibleGrids[i][1] &&
                   grid[w][2] == possibleGrids[i][2] &&
                   grid[w][3] == possibleGrids[i][3] &&
                   grid[w][4] == possibleGrids[i][4]
                   )
                {
                    line = 1;
                }
                i++;
            }
        }
        if(tries == 1)
        {
            while(i != 4)
            {
                if(grid[0][w] == possibleGrids[i][0] &&
                   grid[1][w] == possibleGrids[i][1] &&
                   grid[2][w] == possibleGrids[i][2] &&
                   grid[3][w] == possibleGrids[i][3] &&
                   grid[4][w] == possibleGrids[i][4]
                   )
                {
                    line = 1;
                }
                i++;
            }
        }
        if(tries == 2)
        {
            while(i != 4)
            {
                if(grid[w][4] == possibleGrids[i][0] &&
                   grid[w][3] == possibleGrids[i][1] &&
                   grid[w][2] == possibleGrids[i][2] &&
                   grid[w][1] == possibleGrids[i][3] &&
                   grid[w][0] == possibleGrids[i][4]
                   )
                {
                    line = 1;
                }
                i++;
            }
        }
        if(tries == 3)
        {
            while(i != 4)
            {
                if(grid[4][w] == possibleGrids[i][0] &&
                   grid[3][w] == possibleGrids[i][1] &&
                   grid[2][w] == possibleGrids[i][2] &&
                   grid[1][w] == possibleGrids[i][3] &&
                   grid[0][w] == possibleGrids[i][4]
                   )
                {
                    line = 1;
                }
                i++;
            }
        }
        if(line == 0)
        {
            tries++;
            if(tries == 4)
            {
                return -1;
            }
            w = 0;
        }
    }
    if(tries == 0)
    {
        number = (grid[4][3] | (grid[4][1] << 1) | (grid[3][3] << 2) | (grid[3][1] << 3) | (grid[2][3] << 4) | (grid[2][1] << 5) | (grid[1][3] << 6) | (grid[1][1] << 7) | (grid[0][3] << 8) | (grid[0][1] << 9));
    }
    if(tries == 1)
    {
        number = (grid[0][3] | (grid[0][1] << 1) | (grid[1][3] << 2) | (grid[1][1] << 3) | (grid[2][3] << 4) | (grid[2][1] << 5) | (grid[3][3] << 6) | (grid[3][1] << 7) | (grid[4][3] << 8) | (grid[4][1] << 9));
    }
    if(tries == 2)
    {
        number = (grid[0][1] | (grid[0][3] << 1) | (grid[1][1] << 2) | (grid[1][3] << 3) | (grid[2][1] << 4) | (grid[2][3] << 5) | (grid[3][1] << 6) | (grid[3][3] << 7) | (grid[4][1] << 8) | (grid[4][3] << 9));
    }
    if(tries == 3)
    {
        number = (grid[1][4] | (grid[3][4] << 1) | (grid[1][3] << 2) | (grid[3][3] << 3) | (grid[1][2] << 4) | (grid[3][2] << 5) | (grid[1][1] << 6) | (grid[3][1] << 7) | (grid[1][0] << 8) | (grid[3][0] << 9));
    }
    return number;
}

void vROI(image_t *src,
          image_t *dst,
          uint16_t x,
          uint16_t y,
          uint16_t width,
          uint16_t height)
{
    unsigned i, j;
    if(x + width > src->width || y + height > src->height)
    {
        dst = src;
        return;
    }
    for(i = 0; i < height; i++)
    {
        for (j = 0; j < width; j++)
        {
            dst->data[i][j] = src->data[i+x][j+y];
        }
    }
    dst->lut = src->lut;
    return;
}

void vDrawBlob(image_t *img,
               blobinfo_t blobInfo,
               uint8_t value)
{
    uint16_t i;
    for(i = blobInfo.top; i < blobInfo.bottom + 1; i++)
    {
        img->data[i][blobInfo.left - 1] = value;
        img->data[i][blobInfo.right + 1] = value;
    }
    for(i = blobInfo.left; i < blobInfo.right + 1; i++)
    {
        img->data[blobInfo.top - 1][i] = value;
        img->data[blobInfo.bottom + 1][i] = value;
    }
}


void vSetSelectedBlob(image_t *src, image_t *dst, uint8_t selected, blobinfo_t *blobInfo)
{
    int i, j;
    if(src != dst)
    {
        vCopy(src, dst);
    }
    for(i = blobInfo->top; i < blobInfo->bottom + 1; i++)
    {
        for(j = blobInfo->left; j < blobInfo->right + 1; j++)
        {
            if(dst->data[i][j] == selected)
            {
                dst->data[i][j] = blobInfo->ID;
            }
        }
    }
}

// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
