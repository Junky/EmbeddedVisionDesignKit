#include "VisionSet.h"

void VisionSet2(MainWindow *mw, image_t *pSrc, image_t *pDst)
{    Vector2 point;
     point.x = pSrc->height / 2;
     point.y = pSrc->width / 2;

    vRotateX(pSrc, pDst, 10, point);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("1. vRotateX 10\n"));

    vRotateX(pSrc, pDst, 30, point);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("2. vRotateX 30\n"));

    vRotateX(pSrc, pDst, 45, point);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("3. vRotateX 45\n"));

     point.x = pSrc->height;
     point.y = pSrc->width;
    vRotateX(pSrc, pDst, 45, point);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("3. vRotateX 45\n"));

     point.x = 40;
     point.y = 40;
    vRotateX(pSrc, pDst, 45, point);
    pDst->lut = LUT_CLIP;
    mw->vShowDstImage();
    info.append(QString("3. vRotateX 45\n"));
}
