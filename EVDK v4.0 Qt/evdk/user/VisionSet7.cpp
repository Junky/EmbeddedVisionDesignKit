#include "VisionSet.h"

void VisionSet7(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    // Different methods of filtering all on the same source image pSrc

    vNonlinearFilter(pSrc, pDst, AVERAGE, 5);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("1. vNonlinearFilter AVERAGE 5x5\n"));

    vNonlinearFilter(pSrc, pDst, HARMONIC, 5);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("2. vNonlinearFilter HARMONIC 5x5\n"));

    vNonlinearFilter(pSrc, pDst, MAX, 3);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("3. vNonlinearFilter MAX 3x3\n"));

    vNonlinearFilter(pSrc, pDst, MEDIAN, 5);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("4. vNonlinearFilter MEDIAN 5x5\n"));

    vNonlinearFilter(pSrc, pDst, MIDPOINT, 3);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("5. vNonlinearFilter MIDPOINT 3x3\n"));

    vNonlinearFilter(pSrc, pDst, MIN, 3);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("6. vNonlinearFilter MIN 3x3\n"));

    vNonlinearFilter(pSrc, pDst, RANGE, 3);
    pDst->lut = LUT_STRETCH;
    mw->vShowDstImage();
    info.append(QString("7. vNonlinearFilter RANGE 3x3\n"));
}
