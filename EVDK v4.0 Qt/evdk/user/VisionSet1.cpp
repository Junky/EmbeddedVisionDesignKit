#include "VisionSet.h"

void VisionSet1(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    uint8_t nr=1;
    QDEBUG("Debugging example: VisionSet =" << nr);

//    vNonlinearFilter(pSrc, pDst, AVERAGE, 3);
//    pDst->lut = LUT_CLIP;
//    mw->vShowDstImage();
//    info.append(QString("2. averagefilter 3x3\n"));

//    vNonlinearFilter(pSrc, pDst, HARMONIC, 3);
//    pDst->lut = LUT_CLIP;
//    mw->vShowDstImage();
//    info.append(QString("3. Harmonicfilter 3x3\n"));

//    vNonlinearFilter(pSrc, pDst, MAX, 3);
//    pDst->lut = LUT_CLIP;
//    mw->vShowDstImage();
//    info.append(QString("4. Maxfilter 3x3\n"));

//    vNonlinearFilter(pSrc, pDst, MEDIAN, 3);
//    pDst->lut = LUT_CLIP;
//    mw->vShowDstImage();
//    info.append(QString("5. Medianfilter 3x3\n"));

//    vNonlinearFilter(pSrc, pDst, MIDPOINT, 3);
//    pDst->lut = LUT_CLIP;
//    mw->vShowDstImage();
//    info.append(QString("6. Midpointfilter 3x3\n"));

//    vNonlinearFilter(pSrc, pDst, MIN, 3);
//    pDst->lut = LUT_CLIP;
//    mw->vShowDstImage();
//    info.append(QString("7. Minfilter 3x3\n"));

//    vNonlinearFilter(pSrc, pDst, RANGE, 3);
//    pDst->lut = LUT_CLIP;
//    mw->vShowDstImage();
//    info.append(QString("8. Rangefilter 3x3\n"));

    vThreshold(pSrc, pDst, 0, 100);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("1. vThreshold 0 : 100\n"));

    vRemoveBorderBlobs(pDst, pDst, EIGHT);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("2. vRemoveBorderBlobs EIGHT\n"));

}
