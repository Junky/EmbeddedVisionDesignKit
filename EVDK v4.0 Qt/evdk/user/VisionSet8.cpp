#include "VisionSet.h"

void VisionSet8(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    uint16_t blobcnt;

    vThresholdIsoData(pSrc, pDst, DARK);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("1. vThresholdIsoData DARK\n"));

    vRemoveBorderBlobs(pDst, pDst, EIGHT);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("2. vRemoveBorderBlobs EIGHT\n"));

    vLDF(pDst, pDst);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("3. vLDF\n"));

    blobcnt = iLabelBlobs(pDst, pDst, EIGHT);
    pDst->lut = LUT_LABELED;
    mw->vShowDstImage();
    info.append(QString("4. iLabelBlobs EIGHT\n"));
    info.append(QString("   blobs: %1\n").arg(blobcnt));
}
