#include "VisionSet.h"

void VisionSet3(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    vThreshold(pSrc, pDst, 0, 100);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("1. vThreshold 0 : 100\n"));

    vRemoveBorderBlobs(pDst, pDst, FOUR);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("2. vRemoveBorderBlobs FOUR\n"));

    vFillHoles(pDst, pDst, FOUR);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("3. vFillHoles FOUR\n"));

    vBinaryEdgeDetect(pDst, pDst, FOUR);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("3. vBinaryEdgeDetect FOUR\n"));


}
