#include "VisionSet.h"

void VisionSet9(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    int firstMarkedBlob = -1;
    int foundShotBlob = 0;
    blobinfo_t blobInfo[MAXBLOBS];
    int blobCount;
    int i;
    vThresholdIsoData(pSrc, pDst, DARK);
    mw->vShowDstImage();
    vRemoveBorderBlobs(pDst, pDst, EIGHT);
    mw->vShowDstImage();
    vFillHoles(pDst, pDst, EIGHT);
    mw->vShowDstImage();
    blobCount = iLabelBlobs(pDst, pDst, EIGHT);
    mw->vShowDstImage();
    vBlobAnalyse(pDst, blobCount, blobInfo);
    mw->vShowDstImage();
    blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 50, FSIZE);
    blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 14, FWIDTH);
    blobCount = iFilterBlobs(pDst, pDst, blobInfo, blobCount, 14, FHEIGHT);
    mw->vShowDstImage();
    vGetBlobForm(blobInfo, blobCount);
    mw->vShowDstImage();
    for(i = 0; i < blobCount; i++)
    {
        if(blobInfo[i].form == SQUARE)
        {
            vSetStraight(pSrc, pDst, &blobInfo[i]);
            mw->vShowDstImage();
            vThresholdIsoData(pDst, pDst, DARK);
            mw->vShowDstImage();
            blobInfo[i].ID = 9;
            vSetSelectedBlob(pDst, pDst, 1, &blobInfo[i]);
            mw->vShowDstImage();
            vGetBlobInfo(pDst, blobInfo[i].ID, &blobInfo[i]);
            mw->vShowDstImage();
            vDrawBlob(pDst, blobInfo[i], 2);
            mw->vShowDstImage();
            blobInfo[i].ID = iAruco(pDst, blobInfo[i]);
            QDEBUG(blobInfo[i].ID);
        }
    }
}
