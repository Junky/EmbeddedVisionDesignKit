/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Hugo
 *
 * Description: Header file for all the vision sets
 *
 ******************************************************************************
  Change History:

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#ifndef _VISIONSET_H_
#define _VISIONSET_H_

#include <QString>
#include <QElapsedTimer>
#include "mainwindow.h"
#include "operators.h"
#include "USART.h"

// ----------------------------------------------------------------------------
// Global variables
// ----------------------------------------------------------------------------
extern QString info;

// ----------------------------------------------------------------------------
// Function prototypes
// ----------------------------------------------------------------------------
void VisionSet1(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet2(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet3(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet4(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet5(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet6(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet7(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet8(MainWindow *mw, image_t *pSrc, image_t *pDst);
void VisionSet9(MainWindow *mw, image_t *pSrc, image_t *pDst);

#endif // _VISIONSET_H_
