#include "VisionSet.h"

void VisionSet5(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    vGetAruco(pSrc, pDst);
    pDst->lut = LUT_LABELED;
    mw->vShowDstImage();
    info.append(QString("1. vGetAruco\n"));
}
