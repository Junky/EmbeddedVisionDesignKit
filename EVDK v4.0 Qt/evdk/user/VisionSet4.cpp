#include "VisionSet.h"

void VisionSet4(MainWindow *mw, image_t *pSrc, image_t *pDst)
{
    int blobCount;;

    vThreshold(pSrc, pDst, 0, 100);
    pDst->lut = LUT_BINARY;
    mw->vShowDstImage();
    info.append(QString("1. vThreshold 0 : 100\n"));

    vRemoveBorderBlobs(pDst, pDst, EIGHT);

    blobCount = iLabelBlobs(pDst, pDst, EIGHT);
    pDst->lut = LUT_LABELED;
    mw->vShowDstImage();
    info.append(QString("2. iLabelBlobs EIGHT\n"));
    info.append(QString("   blobCount: ") + QString::number(blobCount) + QString("\n"));

    do
    {
        int i;
        blobinfo_t blobInfo[blobCount];

        vBlobAnalyse(pDst, blobCount, blobInfo);
        pDst->lut = LUT_LABELED;
        mw->vShowDstImage();
        info.append(QString("3. vBlobAnalyse\n"));


        vGetBlobForm(blobInfo, blobCount);
        vCopy(pSrc, pDst);
        vThreshold(pSrc, pDst, 0, 100);
        for(i = 0; i < blobCount; i++)
        {
            if(blobInfo[i].form == SQUARE)
            {
                info.append(QString("Form: Square\n"));
                vDrawBlob(pDst, blobInfo[i], 2);
            }
            else if(blobInfo[i].form == TRIANGLE)
            {
                info.append(QString("Form: Triangle\n"));
                vDrawBlob(pDst, blobInfo[i], 2);
            }
            else if(blobInfo[i].form == CIRCLE)
            {
                info.append(QString("Form: Circle\n"));
                vDrawBlob(pDst, blobInfo[i], 2);
            }
            else
            {
                info.append(QString("Form: Not sure\n"));
            }
        }
        mw->vShowDstImage();

    }
    while(0);
}
