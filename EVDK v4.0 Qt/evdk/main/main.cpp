/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Hugo
 *
 * Description: Implementation file for the main program
 *
 ******************************************************************************
  Change History:

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
//#include <QtGui/QApplication>
#include <QtWidgets/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    w.move(100,20);
    w.show();

    return a.exec();
}
