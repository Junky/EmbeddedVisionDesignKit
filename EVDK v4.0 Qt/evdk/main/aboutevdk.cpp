/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2013 HAN Embedded Systems Engineering
 * Author     : Max van den Beld
 *
 * Description: Source file
 *              Fancy AboutBox with html body
 *              Can be dressed up as pleased
 ******************************************************************************
  Change History:

    Version 1.0 - April 2013
    > Initial revision: Introduced with mainwindow version 2.2

******************************************************************************/
#include "aboutevdk.h"

aboutEvdk::aboutEvdk(QWidget *parent) :
    QWidget(parent)
{
}

void aboutEvdk::setVersion(QString ver)
{
    version = ver;
}

void aboutEvdk::setYearBegin(QString yB)
{
    yearBegin = yB;
}

void aboutEvdk::setYearEnd(QString yE)
{
    yearEnd = yE;
}

void aboutEvdk::setYearCopyright(QString yCr)
{
    yearCopyright = yCr;
}

const char *htmlText1 =
    "<html>"
        "<body>"
            "<h1>"
                "<strong>HAN &ndash; EVDK&nbsp; ";

const char *htmlText2 =
        "</strong><br />"
                "<strong>EVD1 ";


const char *htmlText3 =
        "</strong></h1>"
            "<hr />"
            "<p>"
                "Gebaseerd op:</p>"
            "<ul>"
                "<li>"
                    "Qt Creator</li>"
                "<li>"
                    "Keil MDK-ARM uVision</li>"
            "</ul>"
            "<p>"
                "<br />"
                "Functies:</p>"
            "<ul>"
                "<li>"
                    "30 fps data transfer</li>"
                "<li>"
                    "7 fps (standalone, incl. vision operators)</li>"
                "<li>"
                    "192 kB RAM (multiple pics. 176x144 pixels)</li>"
            "</ul>"
            "<p>"
                "<br />"
                "Installeren:</p>"
            "<ul>"
                "<li>"
                    "<a href=\"http://www.qt.io/download\">Qt Creator</a></li>"
                "<li>"
                    "<a href=\"http://www.keil.com/arm/mdk.asp\">Keil MDK-ARM uVision (lite)</a></li>"
                "<li>"
                    "<a href=\"http://www.st.com/web/en/catalog/tools/PF258168#\">ST -Link utility software (eventueel)</a></li>"
                "<li>"
                    "<a href=\"http://www.ftdichip.com/Drivers/D2XX.htm\">FTDI Chip D2xx driver</a></li>"
            "</ul>"
            "<hr />"
            "<p>"
                "&copy; ";

const char *htmlText4 =
        " HAN ESE minor EVD1: Ontwikkel Vision operatoren in ANSI-C<br />"
                "Project beschikbaargesteld door: H. Arends</p>"
        "</body>"
    "</html>";

void aboutEvdk::showa(void)
{
    QString htmlText;

    htmlText = htmlText1;
    htmlText.append(version);
    htmlText.append(htmlText2).append(yearBegin).append(" - ").append(yearEnd);
    htmlText.append(htmlText3).append(yearCopyright);
    htmlText.append(htmlText4);

    QMessageBox::about(this, "About EVDK...", htmlText);
}
// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
