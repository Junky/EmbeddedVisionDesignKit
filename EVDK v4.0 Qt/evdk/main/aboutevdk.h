/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2013 HAN Embedded Systems Engineering
 * Author     : Max van den Beld
 *
 * Description: Header file
 *              Fancy AboutBox with html body
 *              Can be dressed up as pleased
 ******************************************************************************
  Change History:

    Version 1.0 - April 2013
    > Initial revision: Introduced with mainwindow version 2.2

******************************************************************************/
#ifndef ABOUTEVDK_H
#define ABOUTEVDK_H

#include <QWidget>
#include <QMessageBox>

class aboutEvdk : public QWidget
{
    Q_OBJECT
public:
    explicit aboutEvdk(QWidget *parent = 0);

    void showa(void);
    void setVersion(QString);
    void setYearBegin(QString);
    void setYearEnd(QString);
    void setYearCopyright(QString);

private:
    QString version;
    QString yearBegin;
    QString yearEnd;
    QString yearCopyright;

signals:
    
public slots:
    
};

#endif // ABOUTEVDK_H
// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
