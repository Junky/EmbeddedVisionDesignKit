/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Hugo Arends (Project owner, 2009-2011)
 *              Oscar Starink & Sander Hoevers (EVDK hardware, 2011-2012)
 *              Sjoerd Driessen (Histogram displaying, 2011-2012)
 *              Max van den Beld (Pixelview, Menubar, About,
 *                redesign: mainwindow.ui, 2012-2013)
 *
 * Description: Implementation file for the main program
 *
 ******************************************************************************
  Change History:

    Version 3.1 - November 2014
    > Fix displaying of visionsets after switching to/from target board

    Version 3.0 - October 2014
    > Fix export to JL

    Version 2.2 - April 2014
    > Added functionality: PixelView || Menubar
    > Redesigned: GUI || About.
    > Fix: Disconnect on CloseApp

    Version 2.1 - November 2012
    > Enabled source image LUT
    > Added save options for source and destination images
    > Fixed memory leak when displaying images

    Version 2.0 - October 2012
    > Merged EVDK hardware + histogram

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include "mainwindow.h"
#include "operators.h"
#include "VisionSet.h"
#include "ui_mainwindow.h"

// ----------------------------------------------------------------------------
// Global variables
// ----------------------------------------------------------------------------
QString  info;
uint16_t lut;
uint16_t imgCount;

// ----------------------------------------------------------------------------
// Function implementations
// ----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->labelSrcImageFromEVDK->setVisible(0);

    imgCount = 0;

    // Initialize About
    evdkAbout.setVersion(QString("3.0"));           // Base & subversion
    evdkAbout.setYearBegin(QString("2009"));        // Creation year
    evdkAbout.setYearEnd(QString("2014"));          // Year last modification
    evdkAbout.setYearCopyright(QString("2014"));    // Copyright year

    // Initialize image boundries
    imgSrc.height  = IMG_HEIGHT;
    imgDst.height  = IMG_HEIGHT;
    imgSrc.width   = IMG_WIDTH;
    imgDst.width   = IMG_WIDTH;
    histSrc.height = HIST_HEIGHT;
    histDst.height = HIST_HEIGHT;
    histSrc.width  = HIST_WIDTH;
    histDst.width  = HIST_WIDTH;
    imgDstDisplayed.width  = IMG_WIDTH;
    imgDstDisplayed.height = IMG_HEIGHT;

    for(int y=0; y<(imgSrc.height); y++)
        for(int x=0; x<(imgSrc.width); x++)
            imgSrc.data[y][x] = 255;

    for(int y=0; y<(imgDst.height); y++)
        for(int x=0; x<(imgDst.width); x++)
            imgDst.data[y][x] = 255;

    // Connect click SIGNAL to a SLOT
    connect(ui->gvSrc, SIGNAL(clicked(QMouseEvent *)),
                       SLOT(onPixelView_clicked(QMouseEvent *)));

    connect(ui->gvDst, SIGNAL(clicked(QMouseEvent *)),
                       SLOT(onPixelView_clicked(QMouseEvent *)));


    connect(ui->gvHisSrc, SIGNAL(clicked(QMouseEvent *)),
                          SLOT(onHistogram_clicked(QMouseEvent *)));

    connect(ui->label8ConnValuesSrc, SIGNAL(clicked(QMouseEvent *)),
                          SLOT(onlabel8ConnValuesSrc_clicked(QMouseEvent *)));

    connect(ui->label8ConnValuesDst, SIGNAL(clicked(QMouseEvent *)),
                          SLOT(onlabel8ConnValuesDst_clicked(QMouseEvent *)));


    // Initialize LUT listing
    LUTlist.append("Stretch"); // 0
    LUTlist.append("Clip");    // 1
    LUTlist.append("Binary");  // 2
    LUTlist.append("Labeled"); // 3

    ui->LUTSrc->addItems(LUTlist);
    ui->LUTDst->addItems(LUTlist);

    // Initialize Vision set listing
    VSlist.append("Qt Vision set 1"); // 0
    VSlist.append("Qt Vision set 2"); // 1
    VSlist.append("Qt Vision set 3"); // 2
    VSlist.append("Qt Vision set 4"); // 3
    VSlist.append("Qt Vision set 5"); // 4
    VSlist.append("Qt Vision set 6"); // 5
    VSlist.append("Qt Vision set 7"); // 6
    VSlist.append("Qt Vision set 8"); // 7
    VSlist.append("Qt Vision set 9"); // 8

    ui->Operator->addItems(VSlist);

    ui->LUTSrc->setCurrentIndex(LUT_CLIP);
    DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
    DrawHistogram(&imgSrc, &histSrc);
    DrawHistogramInfoSrc();
    DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

    memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
    DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
    ui->LUTDst->setCurrentIndex(LUT_STRETCH);
    DrawHistogram(&imgDst, &histDst);
    DrawHistogramInfoDst();
    DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

    devKit = NULL;
    ContinuousRunning = false;
    ui->actionClose->setDisabled(true);
    ui->actionOpen->setEnabled(true);

    ui->pushButtonSingle->setVisible(0);

    setMode(RUNMODE_DEFAULT);

    initTableView();

    // Show label8ConnValues
    on_actionPixel_Cursor_Source_triggered(true);
    on_actionPixel_Cursor_Destination_triggered(true);

    QPoint srcPos = ui->gvSrc->mapToGlobal(QPoint(0,0));
    srcPos = ui->horizontalLayoutWidget_5->mapFromGlobal(srcPos);
    QPoint dstPos = ui->gvDst->mapToGlobal(QPoint(0,0));
    dstPos = ui->horizontalLayoutWidget_5->mapFromGlobal(dstPos);

    ui->statusBar->showMessage(QString("(c) 2012 HAN ESE minor EVD1: Develop vision operators in ANSI-C"), 0);
    updateWindowSize();

    executeVisionSet();
}

MainWindow::~MainWindow()
{
    exitClickedHandler();
    delete ui;
}


void MainWindow::aboutClicked()
{
    // Show need QMessagebox with some basics about EVDK
    evdkAbout.showa();
}

void MainWindow::on_actionBbExit_triggered()
{
    exitClickedHandler();
}

void MainWindow::exitClickedHandler()
{
    if(devKit != NULL)
    {
        devKit->close();
    }

    qApp->exit(0);
}

void MainWindow::openImageFile()
{
    switch(runmode)
    {
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        {
            QString filename = QFileDialog::getOpenFileName(this,
                               tr("Open Document"),
                               QDir::currentPath(),
                               tr("Image files (*.bmp *.gif *.jpg *.jpeg *.png);;"));

            if(!filename.isNull())
            {
               int16_t x,y;

               // Read and scale image
               QImage qimage = QImage(filename, 0);
               QImage convert = qimage.convertToFormat(QImage::Format_Indexed8, Qt::AutoColor);
               QImage scaled = convert.scaled(imgSrc.width, imgSrc.height, Qt::IgnoreAspectRatio, Qt::FastTransformation);

               // Convert image
               for(y=0; y<(imgSrc.height); y++)
               {
                   for(x=0; x<(imgSrc.width); x++)
                   {
                      imgSrc.data[y][x] = scaled.pixel(x, y);
                   }
               }

               // Display image
               DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
               DrawHistogram(&imgSrc, &histSrc);
               DrawHistogramInfoSrc();
               DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

               // Recalculate destination image
               executeVisionSet();

               //info.clear();
               //info.append(QString("File succesfully opened and converted to int8 "));
               //info.append(filename);
               //ui->plainTextInfo->appendPlainText(info);
               //info.clear();
            }
            else
            {
               info.clear();
               info.append(QString("Could not open file "));
               info.append(filename);
               ui->plainTextInfo->appendPlainText(info);
            }
        }break;

        // --------------------------------------------------------------------
        case RUNMODE_SNAPSHOT:
        {
            devKit->verbinding.SetImageNr(1);
            devKit->verbinding.SetProgramma(0);
            devKit->verbinding.SetMode(Mode_Single);
            devKit->clearInfo();
        }
        break;

        // --------------------------------------------------------------------
        case RUNMODE_CONTINUOUS:
        {
            if(ContinuousRunning == true)
            {
                devKit->verbinding.SetMode(Mode_None);
                ui->pbOpenImage->setText(QString("Continuous"));
                ContinuousRunning = false;
            }
            else
            {
                devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
                devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
                devKit->verbinding.SetMode(Mode_Continuous);

                ui->pbOpenImage->setText(QString("Stop"));
                ui->plainTextInfo->clear();
                ContinuousRunning = true;
            }
        }break;

        // --------------------------------------------------------------------
        case RUNMODE_FILE_TO_DEVBOARD:
        {

            QString filename = QFileDialog::getOpenFileName(this,
                                                           tr("Open Document"),
                                                           QDir::currentPath(),
                                                           tr("Image files (*.bmp *.gif *.jpg *.jpeg *.png);;"));

            if(!filename.isNull())
            {
               int16_t x,y;

               // Read and scale image
               imgSrc.width = IMG_WIDTH;
               imgSrc.height = IMG_HEIGHT;

               QImage qimage = QImage(filename, 0);
               QImage convert = qimage.convertToFormat(QImage::Format_Indexed8, Qt::AutoColor);
               QImage scaled = convert.scaled(imgSrc.width, imgSrc.height, Qt::IgnoreAspectRatio, Qt::FastTransformation);

               // Convert image
               for(y=0; y<(imgSrc.height); y++)
               {
                   for(x=0; x<(imgSrc.width); x++)
                   {
                      imgSrc.data[y][x] = scaled.pixel(x, y);
                   }
               }

               // Display image
               DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
               DrawHistogram(&imgSrc, &histSrc);
               DrawHistogramInfoSrc();
               DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

               ui->plainTextInfo->clear();
               devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
               devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
               devKit->verbinding.InjectImage(imgSrc);
               devKit->verbinding.SetMode(Mode_Inject);
               devKit->clearInfo();
            }
        }break;

        default:
        {
            ; // Do nothing
        }break;
    }
    updateTableWidgetSrc();
}

void MainWindow::executeVisionSet()
{
    imgCount=0;

    switch(ui->Operator->currentIndex())
    {
    case 0:
    {
        VisionSet1(this, &imgSrc, &imgDst);

    }break;
    case 1:
    {
        VisionSet2(this, &imgSrc, &imgDst);

    }break;
    case 2:
    {
        VisionSet3(this, &imgSrc, &imgDst);

    }break;
    case 3:
    {
        VisionSet4(this, &imgSrc, &imgDst);

    }break;
    case 4:
    {
        VisionSet5(this, &imgSrc, &imgDst);

    }break;
    case 5:
    {
        VisionSet6(this, &imgSrc, &imgDst);

    }break;
    case 6:
    {
        VisionSet7(this, &imgSrc, &imgDst);

    }break;
    case 7:
    {
        VisionSet8(this, &imgSrc, &imgDst);

    }break;
    case 8:
    {
        VisionSet9(this, &imgSrc, &imgDst);

    }break;
    default:
    {

    }break;
    }

    // Display operator info
    ui->plainTextInfo->clear();
    ui->plainTextInfo->appendPlainText(info);
    info.clear();

    // reread pixelvalues into table
    updateTableWidgetDst();

    // update bodytext of pixelView
    ui->label8ConnValuesDst->setText(
                buildPixelViewTxt(
                    QPoint(ui->tableViewDst->currentColumn(),
                           ui->tableViewDst->currentRow()),
                    &imgDstDisplayed ) );
}

void MainWindow::LUTSrcChanged()
{
    DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
    DrawHistogram(&imgSrc, &histSrc);
    DrawHistogramInfoSrc();
    DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);
    updateTableWidgetSrc();
}

void MainWindow::LUTDstChanged()
{
    DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
    //DrawHistogram(&imgDst, &histDst);
    //DrawHistogramInfoDst();
    //DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);
    updateTableWidgetDst();
}

void MainWindow::DrawImage(QGraphicsView *gview, QGraphicsScene *scene, QComboBox *lut, image_t *img)
{
    int16_t x,y;
    uint8_t *rgb = new uint8_t[3 * img->height * img->width];

    // Translate to RGB depending on the LUT
    switch(lut->currentIndex())
    {
    case 1: // Clip
    {
        for(y=0; y<(img->height); y++)
        {
            for(x=0; x<(img->width); x++)
            {
                rgb[(((y*img->width)+x)*3)+0] = img->data[y][x];
                rgb[(((y*img->width)+x)*3)+1] = img->data[y][x];
                rgb[(((y*img->width)+x)*3)+2] = img->data[y][x];
            }
        }
    }break;
    case 2: // Binary
    {
        for(y=0; y<(img->height); y++)
        {
            for(x=0; x<(img->width); x++)
            {
                if(img->data[y][x] == 0)
                {
                    rgb[(((y*img->width)+x)*3)+0] = 255;
                    rgb[(((y*img->width)+x)*3)+1] = 255;
                    rgb[(((y*img->width)+x)*3)+2] = 255;
                }
                else
                {
                    rgb[(((y*img->width)+x)*3)+0] = 0;
                    rgb[(((y*img->width)+x)*3)+1] = 0;
                    rgb[(((y*img->width)+x)*3)+2] = 0;
                }
            }
        }
    }break;
    case 3: // Labeled
    {
        for(y=0; y<(img->height); y++)
        {
            for(x=0; x<(img->width); x++)
            {
                if(img->data[y][x] == 0)
                {
                    // Background
                    rgb[(((y*img->width)+x)*3)+0] = 255;
                    rgb[(((y*img->width)+x)*3)+1] = 255;
                    rgb[(((y*img->width)+x)*3)+2] = 255;
                }
                else if ((img->data[y][x] % 3) == 2)
                {
                    rgb[(((y*img->width)+x)*3)+0] = 0;
                    rgb[(((y*img->width)+x)*3)+1] = 255;
                    rgb[(((y*img->width)+x)*3)+2] = 0;
                }
                else if((img->data[y][x] % 3) == 1)
                {
                    rgb[(((y*img->width)+x)*3)+0] = 255;
                    rgb[(((y*img->width)+x)*3)+1] = 0;
                    rgb[(((y*img->width)+x)*3)+2] = 0;
                }
                else
                {
                    rgb[(((y*img->width)+x)*3)+0] = 0;
                    rgb[(((y*img->width)+x)*3)+1] = 0;
                    rgb[(((y*img->width)+x)*3)+2] = 255;
                }
            }
        }
    }break;
    default: // Stretch
    {
        image_t tmp;

        tmp.height = IMG_HEIGHT;
        tmp.width  = IMG_WIDTH;

        vContrastStretch(img, &tmp, 0, 255);

        for(y=0; y<(tmp.height); y++)
        {
            for(x=0; x<(tmp.width); x++)
            {
                rgb[(((y*tmp.width)+x)*3)+0] = tmp.data[y][x];
                rgb[(((y*tmp.width)+x)*3)+1] = tmp.data[y][x];
                rgb[(((y*tmp.width)+x)*3)+2] = tmp.data[y][x];
            }
        }
    }break;
    }

    // Draw the image
    QImage qimage(rgb, img->width, img->height, QImage::Format_RGB888);
    QPixmap qpixmap = QPixmap::fromImage(qimage);
    scene->clear();
    scene->addPixmap(qpixmap);
    gview->setScene(scene);

    // Clean up
    delete rgb;
}

void MainWindow::DrawHist(QGraphicsView *gview, QGraphicsScene *scene, draw_hist_t *hist)
{
    int16_t x,y;
    uint8_t *rgb = new uint8_t[3 * hist->height * hist->width];

    // Translate hist to RGB (1D -> 3D)
    for(y=0; y<(hist->height); y++)
    {
        for(x=0; x<(hist->width); x++)
        {
            if(hist->data[y][x] == 0)
            {
                // Background
                rgb[ ( ( ( y*hist->width ) + x ) * 3 ) + 0 ] = 255;
                rgb[ ( ( ( y*hist->width ) + x ) * 3 ) + 1 ] = 255;
                rgb[ ( ( ( y*hist->width ) + x ) * 3 ) + 2 ] = 255;
            }
            else if((hist->data[y][x] % 3) == 2)
            {
                rgb[(((y*hist->width)+x)*3)+0] = 0;
                rgb[(((y*hist->width)+x)*3)+1] = 255;
                rgb[(((y*hist->width)+x)*3)+2] = 0;
            }
            else if((hist->data[y][x] % 3) == 1)
            {
                rgb[(((y*hist->width)+x)*3)+0] = 255;
                rgb[(((y*hist->width)+x)*3)+1] = 0;
                rgb[(((y*hist->width)+x)*3)+2] = 0;
            }
            else
            {
                rgb[(((y*hist->width)+x)*3)+0] = 0;
                rgb[(((y*hist->width)+x)*3)+1] = 0;
                rgb[(((y*hist->width)+x)*3)+2] = 255;
            }
        }
    }

    // Draw the image
    QImage qimage(rgb, hist->width, hist->height, QImage::Format_RGB888);
    QPixmap qpixmap = QPixmap::fromImage(qimage);
    scene->clear();
    scene->addPixmap(qpixmap);
    gview->setScene(scene);

    /*
    QGraphicsScene *scene = new QGraphicsScene;
    scene->addPixmap(qpixmap);
    gview->setScene(scene);
    */

    // Clean up
    delete rgb;
}

void MainWindow::saveSrcBmpClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/src.bmp",
                                                    tr("Images (*.bmp);;"));

    if(!filename.isNull())
    {
        QPixmap qpixmap = QPixmap::grabWidget(this->ui->gvSrc);
        QPixmap copy = qpixmap.copy(1,1,176,144);
        copy.save(filename, "BMP");
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveDstBmpClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/dst.bmp",
                                                    tr("Images (*.bmp);;"));

    if(!filename.isNull())
    {
        QPixmap qpixmap = QPixmap::grabWidget(this->ui->gvDst);
        QPixmap copy = qpixmap.copy(1,1,176,144);
        copy.save(filename, "BMP");
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveSrcJlClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/src.jl",
                                                    tr("Document files (*.jl);;"));

    if(!filename.isNull())
    {
        saveJL(&imgSrc, filename);
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveDstJlClicked()
{
    QString filename = QFileDialog::getSaveFileName(this,
                                                    tr("Save Document"),
                                                    QDir::currentPath() + "/dst.jl",
                                                    tr("Document files (*.jl);;"));

    if(!filename.isNull())
    {
        saveJL(&imgDstDisplayed, filename);
    }
    else
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);
    }
}

void MainWindow::saveJL(image_t *img, QString filename)
{
    char p[10];

    QFile file(filename);
    if(!file.open(QIODevice::WriteOnly))
    {
        info.clear();
        info.append(QString("Could not open file "));
        info.append(filename);
        ui->plainTextInfo->appendPlainText(info);

        return;
    }

    file.write("JL ByteImage ");

    itoa(img->height, p, 10);
    file.write(p);
    file.write(" ");

    itoa(img->width, p, 10);
    file.write(p);
    file.write(" ");

    p[0]=0x0A;
    file.write(p, 1);

    file.write((char *)(&img->data[0][0]), img->height*img->width);

    p[0]=0x0A;
    file.write(p, 1);

    file.write("JL");

    file.close();

    info.clear();
    info.append(QString("File successfully exported "));
    info.append(filename);
    ui->plainTextInfo->appendPlainText(info);
}

void MainWindow::on_actionBbHistSrc_triggered()
{
    bool checked = ui->actionShow_histogram_src->isChecked();
    ui->actionShow_histogram_src->setChecked(!checked);
    on_actionShow_histogram_src_triggered(!checked);
}

void MainWindow::on_actionShow_histogram_src_triggered(bool checked)
{
    ui->actionBbHistSrc->setChecked(checked);
    // in order to move the care pixelview with gvDst,
    // retrieve global position groupbox that contains gcDsv
    QPoint movement = ui->grpbxDstImg->mapToGlobal(QPoint(0,0));

    ui->grpbxSrcHist->setVisible(checked);
    updateWindowSize();

    // calculate movement by new global position
    movement -= ui->grpbxDstImg->mapToGlobal(QPoint(0,0));
    movement = ui->label8ConnValuesDst->mapToGlobal(QPoint(0,0)) - movement;
    movement = ui->horizontalLayoutWidget_5->mapFromGlobal(movement);

    // move pixelViewBox
    ui->label8ConnValuesDst->move(movement);
}


void MainWindow::on_actionBbHistDst_triggered()
{
    bool checked = ui->actionShow_histogram_dst->isChecked();
    ui->actionShow_histogram_dst->setChecked(!checked);
    on_actionShow_histogram_dst_triggered(!checked);
}

void MainWindow::on_actionShow_histogram_dst_triggered(bool checked) // laat histogram van de dst image zien.
{
    ui->actionBbHistDst->setChecked(checked);
    ui->grpbxDstHist->setVisible(checked);
    updateWindowSize();
}

// Will resize main window to bodycontent and menubar and statusbar
// All objects should be placed insite a horzontal layout box
void MainWindow::updateWindowSize(void)
{
    QSize newSize;

    // adjust size of source image window
    ui->verticalLayoutWidget->adjustSize();

    // adjust size window placement horizontal
    ui->horizontalLayoutWidget_5->adjustSize();

    // retrieve widget size and add all objects
    newSize  = ui->horizontalLayoutWidget_5->frameSize();
    newSize += QSize(0, menuBar()->height());
    newSize += QSize(0, menuWidget()->height());
    newSize += QSize(0, statusBar()->height() + 6);  // add statusbar margin too

    setMinimumSize(newSize);
//  setMaximumSize(newSize);

    adjustSize();
}

void MainWindow::DrawHistogramInfoSrc(void) // laat juiste informatie zien in de labels die bij de histogram van de src hoort.
{
    QString highSrc;
    QString meanSrc;
    QString minSrc;
    QString maxSrc;

    highSrc = QString("%1 -").arg(histSrc.high);
    ui->labelHoogSrc->setText(highSrc);

    highSrc = QString("%1 -").arg(histSrc.high/2);
    ui->labelMiddelSrc->setText(highSrc);

    meanSrc = QString("Mean: %1").arg(histSrc.mean);
    ui->labelMeanSrc->setText(meanSrc);

    minSrc = QString("MinVal: %1").arg(histSrc.min);
    ui->labelMinSrc->setText(minSrc);

    maxSrc = QString("MaxVal: %1").arg(histSrc.max);
    ui->labelMaxSrc->setText(maxSrc);
}

void MainWindow::DrawHistogramInfoDst() // laat juiste informatie zien in de labels die bij de histogram van de dst hoort.
{
    QString highDst;
    QString meanDst;
    QString minDst;
    QString maxDst;

    highDst = QString("%1 -").arg(histDst.high);
    ui->labelHoogDst->setText(highDst);

    highDst = QString("%1 -").arg(histDst.high/2);
    ui->labelMiddelDst->setText(highDst);

    meanDst = QString("Mean: %1").arg(histDst.mean);
    ui->labelMeanDst->setText(meanDst);

    minDst = QString("MinVal: %1").arg(histDst.min);
    ui->labelMinDst->setText(minDst);

    maxDst = QString("MaxVal: %1").arg(histDst.max);
    ui->labelMaxDst->setText(maxDst);
}

void MainWindow::DrawHistogram(image_t *src, draw_hist_t *hist)
{
    int iaHist[256] = {0};
    int i, j;
    int teller  = 0, hoogte = 0, laagste = 255,
        hoogste = 0, divide = 1, gemiddelde = 0;
    int screenHeight = ui->gvHisSrc->size().height() + 5;

    // Loop through the image to make the histogram and extract
    // lowest & highest pixel value
    for(i = 0; i < src->height; i++)
    {
        for(j = 0; j < src->width; j++)
        {
            iaHist[src->data[i][j]] += 1;

            if(src->data[i][j] < laagste)
            {
                laagste = src->data[i][j];
            }

            if(src->data[i][j] > hoogste)
            {
                hoogste = src->data[i][j];
            }
        }
    }

    // Loop through histogram to determine height and mean value
    for(i = 0; i < 256; i++)
    {
        if(iaHist[i] > hoogte)
        {
            hoogte = iaHist[i];
        }
        gemiddelde += iaHist[i] * i;
        teller += iaHist[i];
    }

    gemiddelde = gemiddelde / teller;

    if((hoogte / screenHeight) >= 1)
    {
        divide = hoogte / screenHeight;
    }

    // Make an image of the histogram
    for(i = 0; i < 256; i++)
    {
        iaHist[i] /= divide;
    }

    for(i = 0; i < hist->width; i++)
    {
        teller = 0;
        for(j = hist->height - 1; j >= 0; j--)
        {
            if(teller < iaHist[i])
            {
                hist->data[j][i] = 3;
            }
            else
            {
                hist->data[j][i] = 0;
            }
            teller++;
        }
    }

    hist->max  = hoogste;
    hist->min  = laagste;
    hist->high = hoogte;
    hist->mean = gemiddelde;
}

void MainWindow::onHistogram_clicked(QMouseEvent *event)
{
    int x; //, y;

    // Is the source image histogram displayed?
    if(ui->actionShow_histogram_src->isChecked())
    {
        // Yes, it is at absolute position:
        x = event->pos().x();
      //y = event->pos().y();

        // Is the click event in the gv of the Histogram?
        if( ui->gvHisSrc->underMouse())
        {
            // clear info screenText
            info.clear();
            ui->plainTextInfo->clear();

            // Left mouse button clicked?
            if(event->button() == Qt::LeftButton)
            {
                // Yes, threshold from 0 to x
                vThreshold(&imgSrc, &imgDst, 0, x);
                info.append(QString("1. Threshold 0 - %1\n").arg(x));
            }
            // Right mouse button clicked?
            else if(event->button() == Qt::RightButton)
            {
                // Yes, threshold from x to 255
                vThreshold(&imgSrc, &imgDst, x, 255);
                info.append(QString("1. Threshold %1 - 255\n").arg(x));
            }

            // Draw resulting threshold image in destination graph
            memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
            DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
            DrawHistogram(&imgDst, &histDst);
            DrawHistogramInfoDst();
            DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

            lut = LUT_BINARY;
            ui->LUTDst->setCurrentIndex(lut);

            ui->plainTextInfo->appendPlainText(info);
        }
    }
}

void MainWindow::on_actionOpen_triggered()
{
    if(devKit == NULL)
    {
        ui->actionClose->setEnabled(true);
        ui->actionOpen->setDisabled(true);

        devKit = new ft245app;
        devKit->move(850,450);
        devKit->show();

        connect(devKit, SIGNAL(destroyed()),this, SLOT(destroyedemitted()));

        connect(&(devKit->verbinding.object), SIGNAL(NewImage(int,image_t)), this, SLOT(NewImage(int,image_t)));
        connect(&(devKit->verbinding.object), SIGNAL(NewInfoStr(QString)), this, SLOT(NewInfo(QString)));

        setMode(RUNMODE_SNAPSHOT);
        updateWindowSize();
    }
}

void MainWindow::on_actionClose_triggered()
{
    if(devKit != NULL)
    {
        devKit->close();
    }

    setMode(RUNMODE_DEFAULT);
}

void MainWindow::menuButtonEvdkControlSetVisible(bool yes)
{
    ui->actionBbFile->setVisible(yes);
    ui->actionBbSnapshot->setVisible(yes);
    ui->actionBbContinuous->setVisible(yes);
}

void MainWindow::destroyedemitted()
{
    ui->actionClose->setDisabled(true);
    ui->actionOpen->setEnabled(true);

    devKit = NULL;

    setMode(RUNMODE_DEFAULT);
}

void MainWindow::NewImage(int nr, image_t img)
{
    nr=nr;

    switch(runmode)
    {
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        {
            ; // Do nothing
        }break;

        // --------------------------------------------------------------------
        // Receive snapshot from devboard and display as source image
        // Execute visionset so the destinition image is calculated by the
        // PC vision operators
        case RUNMODE_SNAPSHOT:
        {
            memcpy(&imgSrc, &img, sizeof(image_t));

            DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
            DrawHistogram(&imgSrc, &histSrc);
            DrawHistogramInfoSrc();
            DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

            executeVisionSet();
        }break;

        // --------------------------------------------------------------------
        // Vision operations are executed on the devboard, so this is the
        // destination image
        case RUNMODE_CONTINUOUS:
        case RUNMODE_FILE_TO_DEVBOARD:
        {
            memcpy(&imgDst, &img, sizeof(image_t));

            // Set LUT for destination image
            ui->LUTDst->setCurrentIndex(img.lut);

            memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
            DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
            DrawHistogram(&imgDst, &histDst);
            DrawHistogramInfoDst();
            DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);
        }break;

        // --------------------------------------------------------------------
        default:
        {
            ; // Do nothing
        }
    }
}

void MainWindow::NewInfo(QString str)
{
    switch(runmode)
    {
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        case RUNMODE_SNAPSHOT:
        {
            ; // Do nothing
        }break;

        // --------------------------------------------------------------------
        case RUNMODE_CONTINUOUS:
        case RUNMODE_FILE_TO_DEVBOARD:
        {
            ui->plainTextInfo->appendPlainText(str);
        }break;

        // --------------------------------------------------------------------
        default:
        {
            ; // Do nothing
        }
    }
}

void MainWindow::on_actionBbSnapshot_triggered()
{
    on_actionSnapshot_triggered();
}

void MainWindow::on_actionSnapshot_triggered()
{
    if(devKit != NULL)
    {
        devKit->verbinding.SetMode(Mode_None);
    }

    vErase(&imgSrc);
    DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
    DrawHistogram(&imgSrc, &histSrc);
    DrawHistogramInfoSrc();
    DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

    vErase(&imgDst);
    memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
    DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
    DrawHistogram(&imgDst, &histDst);
    DrawHistogramInfoDst();
    DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

    setMode(RUNMODE_SNAPSHOT);
}


void MainWindow::on_actionBbFile_triggered()
{
    on_actionFile_to_DevBoard_triggered();
}

void MainWindow::on_actionFile_to_DevBoard_triggered()
{
    if(runmode != RUNMODE_FILE_TO_DEVBOARD)
    {
        if(devKit != NULL)
        {
            devKit->verbinding.SetMode(Mode_None);
        }

        vErase(&imgSrc);
        DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
        DrawHistogram(&imgSrc, &histSrc);
        DrawHistogramInfoSrc();
        DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

        vErase(&imgDst);
        memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
        DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
        DrawHistogram(&imgDst, &histDst);
        DrawHistogramInfoDst();
        DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

        setMode(RUNMODE_FILE_TO_DEVBOARD);
    }
}


void MainWindow::on_actionBbContinuous_triggered()
{
    on_actionContinuous_triggered();
}

void MainWindow::on_actionContinuous_triggered()
{
    if(runmode != RUNMODE_CONTINUOUS)
    {
        ContinuousRunning = false;
        if(devKit != NULL)
        {
            devKit->verbinding.SetMode(Mode_None);
        }

        vErase(&imgSrc);
        DrawImage(ui->gvSrc, &sceneSrcImg, ui->LUTSrc, &imgSrc);
        DrawHistogram(&imgSrc, &histSrc);
        DrawHistogramInfoSrc();
        DrawHist(ui->gvHisSrc, &sceneSrcHist, &histSrc);

        vErase(&imgDst);
        memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
        DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
        DrawHistogram(&imgDst, &histDst);
        DrawHistogramInfoDst();
        DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);

        setMode(RUNMODE_CONTINUOUS);

        // Force button press openImageFile to start running live
        openImageFile();
    }
}


void MainWindow::setMode(runmode_t m)
{
    // default setting that will be altered by RUNMODE_CONTINUOUS
    if( runmode == RUNMODE_CONTINUOUS )
    {
        ui->actionCursor_Source->setEnabled(true);
        ui->actionPixel_Cursor_Source->setEnabled(true);
        ui->actionPixel_Table_Source->setEnabled(true);
        ui->actionTable_Source->setEnabled(true);

        ui->actionShow_histogram_src->setEnabled(true);
        ui->actionBbHistSrc->setEnabled(true);
        on_actionShow_histogram_src_triggered(ui->actionShow_histogram_src->isChecked());

        QStringList list;
        list.append("Qt Vision set 1"); // 0
        list.append("Qt Vision set 2"); // 1
        list.append("Qt Vision set 3"); // 2
        list.append("Qt Vision set 4"); // 3
        list.append("Qt Vision set 5"); // 4
        list.append("Qt Vision set 6"); // 5
        list.append("Qt Vision set 7"); // 6
        list.append("Qt Vision set 8"); // 7
        list.append("Qt Vision set 9"); // 8
        ui->Operator->clear();
        ui->Operator->addItems(list);
    }

    runmode = m;

    switch(runmode)
    {
        // Simulation mode
        // --------------------------------------------------------------------
        case RUNMODE_DEFAULT:
        {
            ui->statusBar->showMessage(QString("Mode DEFAULT: file processed with Qt operators"));

            ui->actionSnapshot->setEnabled(0);
            ui->actionSnapshot->setChecked(0);
            ui->actionBbSnapshot->setEnabled(0);
            ui->actionBbSnapshot->setChecked(0);
            ui->actionBbSnapshot->setVisible(0);

            ui->actionContinuous->setEnabled(0);
            ui->actionContinuous->setChecked(0);
            ui->actionBbContinuous->setEnabled(0);
            ui->actionBbContinuous->setChecked(0);
            ui->actionBbContinuous->setVisible(0);

            ui->actionFile_to_DevBoard->setChecked(0);
            ui->actionFile_to_DevBoard->setEnabled(0);
            ui->actionBbFile->setEnabled(0);
            ui->actionBbFile->setChecked(0);
            ui->actionBbFile->setVisible(0);

            ui->pbOpenImage->setText(QString("Open Image"));

            ui->pushButtonSingle->setVisible(0);

            ui->spinBoxDevKit->setVisible(1);

            ui->gvSrc->setVisible(1);
            ui->labelSrcImageFromEVDK->setVisible(0);

            ui->label8ConnValuesSrc->hide();
            ui->grpbxSrcTable->hide();

            QStringList list;
            list.append("Qt Vision set 1"); // 0
            list.append("Qt Vision set 2"); // 1
            list.append("Qt Vision set 3"); // 2
            list.append("Qt Vision set 4"); // 3
            list.append("Qt Vision set 5"); // 4
            list.append("Qt Vision set 6"); // 5
            list.append("Qt Vision set 7"); // 6
            list.append("Qt Vision set 8"); // 7
            list.append("Qt Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);

        }break;

        // Board mode
        // --------------------------------------------------------------------
        case RUNMODE_SNAPSHOT:
        {
            ui->statusBar->showMessage(QString("Mode SNAPSHOT: snapshot from EVDK processed with Qt operators"));

            ui->actionSnapshot->setEnabled(1);
            ui->actionSnapshot->setChecked(1);
            ui->actionBbSnapshot->setEnabled(1);
            ui->actionBbSnapshot->setChecked(1);
            ui->actionBbSnapshot->setVisible(1);

            ui->actionContinuous->setEnabled(1);
            ui->actionContinuous->setChecked(0);
            ui->actionBbContinuous->setEnabled(1);
            ui->actionBbContinuous->setChecked(0);
            ui->actionBbContinuous->setVisible(1);

            ui->actionFile_to_DevBoard->setEnabled(1);
            ui->actionFile_to_DevBoard->setChecked(0);
            ui->actionBbFile->setEnabled(1);
            ui->actionBbFile->setChecked(0);
            ui->actionBbFile->setVisible(1);

            ui->pbOpenImage->setText(QString("Get snapshot from EVDK"));

            ui->pushButtonSingle->setVisible(0);

            ui->spinBoxDevKit->setVisible(1);

            ui->gvSrc->setVisible(1);
            ui->labelSrcImageFromEVDK->setVisible(0);

            ui->label8ConnValuesSrc->setVisible(ui->actionPixel_Cursor_Source->isChecked());
            ui->grpbxSrcTable->setVisible(ui->actionPixel_Table_Source->isChecked());

            QStringList list;
            list.append("Qt Vision set 1"); // 0
            list.append("Qt Vision set 2"); // 1
            list.append("Qt Vision set 3"); // 2
            list.append("Qt Vision set 4"); // 3
            list.append("Qt Vision set 5"); // 4
            list.append("Qt Vision set 6"); // 5
            list.append("Qt Vision set 7"); // 6
            list.append("Qt Vision set 8"); // 7
            list.append("Qt Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);

        }break;

        // Board mode
        // --------------------------------------------------------------------
        case RUNMODE_CONTINUOUS:
        {
            ui->statusBar->showMessage(QString("Mode CONTINUOUS: snapshot from EVDK processed with EVDK operators"));

            if(ContinuousRunning == true)
            {
                ui->pbOpenImage->setText(QString("Stop"));
            }
            else
            {
                ui->pbOpenImage->setText(QString("Continuous"));
            }

            ui->actionSnapshot->setEnabled(1);
            ui->actionSnapshot->setChecked(0);
            ui->actionBbSnapshot->setEnabled(1);
            ui->actionBbSnapshot->setChecked(0);
            ui->actionBbSnapshot->setVisible(1);

            ui->actionContinuous->setEnabled(1);
            ui->actionContinuous->setChecked(1);
            ui->actionBbContinuous->setEnabled(1);
            ui->actionBbContinuous->setChecked(1);
            ui->actionBbContinuous->setVisible(1);

            ui->actionFile_to_DevBoard->setEnabled(1);
            ui->actionFile_to_DevBoard->setChecked(0);
            ui->actionBbFile->setEnabled(1);
            ui->actionBbFile->setChecked(0);
            ui->actionBbFile->setVisible(1);


            ui->pushButtonSingle->setVisible(1);
            ui->pushButtonSingle->setText(QString("Single"));

            ui->spinBoxDevKit->setVisible(1);

            // Hide source image, because there is only one image in continuous mode
            ui->gvSrc->setVisible(0);
            ui->labelSrcImageFromEVDK->setVisible(1);

            // disable buttons
            ui->actionCursor_Source->setEnabled(false);
            ui->actionPixel_Cursor_Source->setEnabled(false);
            ui->actionPixel_Table_Source->setEnabled(false);
            ui->actionTable_Source->setEnabled(false);

            ui->actionShow_histogram_src->setEnabled(false);
            ui->actionBbHistSrc->setEnabled(false);

            // hide pixelview source
            ui->actionCursor_Source->setEnabled(false);
            ui->label8ConnValuesSrc->hide();
            ui->grpbxSrcTable->hide();

            // hide histogram source if visable
            if(ui->actionShow_histogram_src->isChecked())
            {
//                ui->actionBbHistSrc->setEnabled(false);
//                ui->actionShow_histogram_src->setChecked(false);
                on_actionShow_histogram_src_triggered(false);
            }

            QStringList list;
            list.append("EVDK Vision set 1"); // 0
            list.append("EVDK Vision set 2"); // 1
            list.append("EVDK Vision set 3"); // 2
            list.append("EVDK Vision set 4"); // 3
            list.append("EVDK Vision set 5"); // 4
            list.append("EVDK Vision set 6"); // 5
            list.append("EVDK Vision set 7"); // 6
            list.append("EVDK Vision set 8"); // 7
            list.append("EVDK Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);

        }break;

        // Board mode
        // --------------------------------------------------------------------
        case RUNMODE_FILE_TO_DEVBOARD:
        {
            ui->statusBar->showMessage(QString("Mode FILE_TO_DEVBOARD: file processed with EVDK operators"));

            ui->actionSnapshot->setEnabled(1);
            ui->actionSnapshot->setChecked(0);
            ui->actionBbSnapshot->setEnabled(1);
            ui->actionBbSnapshot->setChecked(0);
            ui->actionBbSnapshot->setVisible(1);

            ui->actionContinuous->setEnabled(1);
            ui->actionContinuous->setChecked(0);
            ui->actionBbContinuous->setEnabled(1);
            ui->actionBbContinuous->setChecked(0);
            ui->actionBbContinuous->setVisible(1);

            ui->actionFile_to_DevBoard->setEnabled(1);
            ui->actionFile_to_DevBoard->setChecked(1);
            ui->actionBbFile->setEnabled(1);
            ui->actionBbFile->setChecked(1);
            ui->actionBbFile->setVisible(1);


            ui->pbOpenImage->setText(QString("Open Image"));

            ui->pushButtonSingle->setVisible(1);
            ui->pushButtonSingle->setText(QString("Send Again"));

            ui->spinBoxDevKit->setVisible(1);

            ui->gvSrc->setVisible(1);
            ui->labelSrcImageFromEVDK->setVisible(0);

            ui->label8ConnValuesSrc->setVisible(ui->actionPixel_Cursor_Source->isChecked());
            ui->grpbxSrcTable->setVisible(ui->actionPixel_Table_Source->isChecked());

            QStringList list;
            list.append("EVDK Vision set 1"); // 0
            list.append("EVDK Vision set 2"); // 1
            list.append("EVDK Vision set 3"); // 2
            list.append("EVDK Vision set 4"); // 3
            list.append("EVDK Vision set 5"); // 4
            list.append("EVDK Vision set 6"); // 5
            list.append("EVDK Vision set 7"); // 6
            list.append("EVDK Vision set 8"); // 7
            list.append("EVDK Vision set 9"); // 8
            ui->Operator->clear();
            ui->Operator->addItems(list);

        }break;

        // --------------------------------------------------------------------
        default:
        {
            ; // Do nothing
        }
    }
}

void MainWindow::closeEvent(QCloseEvent * event)
{
    event->accept();

    if(devKit != NULL)
        devKit->close();
}

void MainWindow::on_Operator_currentIndexChanged(int index)
{
    ui->spinBoxDevKit->setValue(1);

    if(devKit != NULL)
    {
        devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
        devKit->verbinding.SetProgramma(index+1);
    }

    if(runmode == RUNMODE_DEFAULT || runmode == RUNMODE_SNAPSHOT)
    {
        executeVisionSet();
    }
}

void MainWindow::on_spinBoxDevKit_valueChanged(int index)
{
    if(devKit != NULL)
    {
        devKit->verbinding.SetImageNr(index);

        if(runmode == RUNMODE_SNAPSHOT)
        {
            devKit->verbinding.SetMode(Mode_None);
            ContinuousRunning = false;

            ui->plainTextInfo->clear();
            devKit->clearInfo();

            executeVisionSet();
        }
    }
    else
    {
        executeVisionSet();
    }
}

void MainWindow::on_pushButtonSingle_clicked()
{
    if(runmode == RUNMODE_CONTINUOUS)
    {
        devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
        devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
        devKit->verbinding.SetMode(Mode_Single);

        ui->pbOpenImage->setText(QString("Continuous"));
        ui->plainTextInfo->clear();
        ContinuousRunning = false;
        devKit->clearInfo();
    }
    else if(runmode == RUNMODE_FILE_TO_DEVBOARD)
    {
        ui->plainTextInfo->clear();
        devKit->verbinding.SetImageNr(ui->spinBoxDevKit->value());
        devKit->verbinding.SetProgramma(ui->Operator->currentIndex()+1);
        devKit->verbinding.SetMode(Mode_Inject);
        devKit->clearInfo();
    }
}

void MainWindow::vShowDstImage()
{
    imgCount++;

    if((imgCount != 0) && (imgCount == ui->spinBoxDevKit->value()))
    {
        // Set LUT for destination image
        ui->LUTDst->setCurrentIndex(imgDst.lut);

        // Draw the image
        memcpy(&imgDstDisplayed, &imgDst, sizeof(image_t));
        DrawImage(ui->gvDst, &sceneDstImg, ui->LUTDst, &imgDstDisplayed);
        DrawHistogram(&imgDst, &histDst);
        DrawHistogramInfoDst();
        DrawHist(ui->gvHisDst, &sceneDstHist, &histDst);
    }
}

///============
void MainWindow::onPixelView_clicked( QMouseEvent *e )
{
    updatePixelView(e->pos());
}
void MainWindow::onlabel8ConnValuesSrc_clicked(QMouseEvent *e)
{
    // reCalculate event position from overlaying QLabel to QGraphicsView
    QPoint gvPos = ui->label8ConnValuesSrc->mapToGlobal(e->pos());
    gvPos = ui->gvSrc->mapFromGlobal(gvPos);
    gvPos -= QPoint(1,0); // some offset!?

    updatePixelView(gvPos);
}

void MainWindow::onlabel8ConnValuesDst_clicked(QMouseEvent *e)
{
    // reCalculate event position from overlaying QLabel to QGraphicsView
    QPoint gvPos = ui->label8ConnValuesDst->mapToGlobal(e->pos());
    gvPos = ui->gvDst->mapFromGlobal(gvPos);
    gvPos -= QPoint(1,0); // some offset!?

    updatePixelView(gvPos);
}

void MainWindow::updatePixelView(QPoint e)
{
    // one pixel offset in order to point at the clicked pixel
    QPoint pixelOffset = QPoint(1,1);

    // reCalculate mouse position from QGraphicsView to MainWindow
    QPoint srcPos = ui->gvSrc->mapToGlobal(e);
    srcPos = ui->horizontalLayoutWidget_5->mapFromGlobal(srcPos);

    QPoint dstPos = ui->gvDst->mapToGlobal(e);
    dstPos = ui->horizontalLayoutWidget_5->mapFromGlobal(dstPos);

    // add offset so the corner of the pixelView will point at the selected pixel
    srcPos += pixelOffset;
    dstPos += pixelOffset;

    bool inRangeSrc = (e.x() <= ui->gvSrc->width()) && (e.y() <= ui->gvSrc->height());
    bool inRangeDst = (e.x() <= ui->gvDst->width()) && (e.y() <= ui->gvDst->height());

    if( inRangeSrc && (
            ui->actionSynchronize_Source_Destination->isChecked() ||
            ui->gvSrc->underMouse() ||
            ui->label8ConnValuesSrc->underMouse() ))
    {
        // update bodytext of pixelView
        ui->label8ConnValuesSrc->setText( buildPixelViewTxt( e, &imgSrc ) );

        // move pixelViewBox underneath mouse
        ui->label8ConnValuesSrc->move(srcPos);

        // select pixel in tableView
        ui->tableViewSrc->setCurrentCell( e.y(), e.x() );
    }

    if( inRangeDst && (
            ui->actionSynchronize_Source_Destination->isChecked() ||
            ui->gvDst->underMouse() ||
            ui->label8ConnValuesDst->underMouse() ))
    {
        // update bodytext of pixelView
        ui->label8ConnValuesDst->setText( buildPixelViewTxt( e, &imgDstDisplayed ) );

        // move pixelViewBox underneath mouse
        ui->label8ConnValuesDst->move(dstPos);

        // select pixel in tableView
        ui->tableViewDst->setCurrentCell( e.y(), e.x() );
    }
}

QString MainWindow::buildPixelViewTxt(QPoint msPos, image_t *img)
{
    QString eightConnectedTxt, srcDst;

    if( img == &imgSrc )
        srcDst.append("Src");
    else
        srcDst.append("Dst");

    // maintain boundries to prevent reading beond array dimentions
    if( msPos.x() <= 0 )
        msPos.setX(1);

    if( msPos.x() >= img->width )
        msPos.setX( img->width - 1 );

    if( msPos.y() <= 0 )
        msPos.setY(1);

    if( msPos.y() >= img->height )
        msPos.setY( img->height - 1 );

    eightConnectedTxt = "";


    // to build this box

    ///////////////////
    //Src ___ 000 ___//
    //   |000|000|000//
    //000 000|000|000//
    //   |000|000|000//
    ///////////////////


    // data is to be created like this:

    //Src ___ 000 ___\n   |000|000|000\n000 000|000|000\n   |000|000|000\n


    // it is achieved by these lines of code

    eightConnectedTxt.append(QString("%1 ___ %2 ___\n")
                     .arg(srcDst)
                     .arg(msPos.x(), 3, 10, QChar('0')));

    eightConnectedTxt.append(QString("   |%1|%2|%3\n")
                     .arg(img->data[msPos.y()-1][msPos.x()-1], 3, 10, QChar('0'))
                     .arg(img->data[msPos.y()-1][msPos.x()  ], 3, 10, QChar('0'))
                     .arg(img->data[msPos.y()-1][msPos.x()+1], 3, 10, QChar('0')));

    eightConnectedTxt.append(QString("%1 ")
                     .arg(msPos.y(), 3, 10, QChar('0')));

    eightConnectedTxt.append(QString("%1|%2|%3\n")
                     .arg(img->data[msPos.y()  ][msPos.x()-1], 3, 10, QChar('0'))
                     .arg(img->data[msPos.y()  ][msPos.x()  ], 3, 10, QChar('0'))
                     .arg(img->data[msPos.y()  ][msPos.x()+1], 3, 10, QChar('0')));

    eightConnectedTxt.append(QString("   |%1|%2|%3")
                     .arg(img->data[msPos.y()+1][msPos.x()-1], 3, 10, QChar('0'))
                     .arg(img->data[msPos.y()+1][msPos.x()  ], 3, 10, QChar('0'))
                     .arg(img->data[msPos.y()+1][msPos.x()+1], 3, 10, QChar('0')));

    return eightConnectedTxt;
}

void MainWindow::initTableView(void)
{
    QStringList rowHeader, columnHeader;

    // don't show tables
    ui->grpbxSrcTable->hide();
    ui->grpbxDstTable->hide();

    // set sizing of tableView items
    ui->tableViewSrc->setColumnCount(imgSrc.width);
    ui->tableViewSrc->setRowCount(imgSrc.height);

    ui->tableViewDst->setColumnCount(imgSrc.width);
    ui->tableViewDst->setRowCount(imgSrc.height);

    // fill items with image data
    for( int row = 0; row < imgSrc.height; row++ )
    {
        for( int column = 0; column < imgSrc.width; column++ )
        {
            ui->tableViewSrc->setItem( row, column, new QTableWidgetItem( QString("---")));
            ui->tableViewDst->setItem( row, column, new QTableWidgetItem( QString("---")));
            columnHeader << QString("%1").arg(column);
        }
        rowHeader << QString("%1").arg(row);
    }

    // set labels
    ui->tableViewSrc->setHorizontalHeaderLabels(columnHeader);
    ui->tableViewSrc->setVerticalHeaderLabels(rowHeader);

    ui->tableViewDst->setHorizontalHeaderLabels(columnHeader);
    ui->tableViewDst->setVerticalHeaderLabels(rowHeader);

    // some formatting
    ui->tableViewSrc->resizeColumnsToContents();
    ui->tableViewSrc->resizeRowsToContents();

    ui->tableViewDst->resizeColumnsToContents();
    ui->tableViewDst->resizeRowsToContents();
}

void MainWindow::updateTableWidgetSrc(void)
{
    // make sure sizing is ok
    if( ui->tableViewSrc->columnCount() >= imgSrc.width &&
            ui->tableViewSrc->rowCount() >= imgSrc.height )
        // fill items with image data
        for( int row = 0; row < imgSrc.height; row++ )
            for( int column = 0; column < imgSrc.width; column++ )
                ui->tableViewSrc->item( row, column)->setText( QString("%1")
                                                                 .arg(imgSrc.data[row][column]));
}

void MainWindow::updateTableWidgetDst(void)
{
    // make sure sizing is ok
    if( ui->tableViewDst->columnCount() >= imgDst.width &&
            ui->tableViewDst->rowCount() >= imgDst.height )
        // fill items with image data
        for( int row = 0; row < imgDst.height; row++ )
            for( int column = 0; column < imgDst.width; column++ )
                ui->tableViewDst->item( row, column)->setText( QString("%1")
                                                                 .arg(imgDstDisplayed.data[row][column]));
}

void MainWindow::on_actionPixel_Table_Source_triggered(bool checked)
{
    // in order to move the care pixelview with gvDst,
    // retrieve global position groupbox that contains gcDsv
    QPoint movement = ui->grpbxDstImg->mapToGlobal(QPoint(0,0));

//    ui->grpbxSrcHist->setVisible(!checked && ui->actionShow_histogram_src->isChecked());

    // hide or show source pixelview
    ui->grpbxSrcTable->setVisible(checked);
    ui->actionTable_Source->setChecked(checked);

    // resize main window
    updateWindowSize();

    // reread image data into table
    updateTableWidgetSrc();

    // calculate movement by new global position
    movement -= ui->grpbxDstImg->mapToGlobal(QPoint(0,0));
    movement = ui->label8ConnValuesDst->mapToGlobal(QPoint(0,0)) - movement;
    movement = ui->horizontalLayoutWidget_5->mapFromGlobal(movement);

    // move pixelViewBox
    ui->label8ConnValuesDst->move(movement);
}

void MainWindow::on_actionPixel_Table_Destination_triggered(bool checked)
{
//    ui->grpbxDstHist->setVisible(!checked && ui->actionShow_histogram_dst->isChecked());
    ui->grpbxDstTable->setVisible(checked);
    ui->actionTable_Destination->setChecked(checked);
    updateWindowSize();
}

void MainWindow::on_actionTable_Source_triggered(bool checked)
{
    ui->actionPixel_Table_Source->setChecked(checked);
    on_actionPixel_Table_Source_triggered(checked);
}


void MainWindow::on_actionTable_Destination_triggered(bool checked)
{
    ui->actionPixel_Table_Destination->setChecked(checked);
    on_actionPixel_Table_Destination_triggered(checked);
}

// cursor
void MainWindow::on_actionPixel_Cursor_Source_triggered(bool checked)
{
    QPoint srcPos;
    srcPos = ui->label8ConnValuesDst->mapToGlobal(QPoint(0,0));
    srcPos -= ui->gvDst->mapToGlobal(QPoint(0,0));
    srcPos += ui->gvSrc->mapToGlobal(QPoint(0,0));
    srcPos = ui->horizontalLayoutWidget_5->mapFromGlobal(srcPos);

    ui->actionCursor_Source->setChecked(checked);
    ui->label8ConnValuesSrc->move(srcPos);
    ui->label8ConnValuesSrc->setVisible(checked);
}

void MainWindow::on_actionPixel_Cursor_Destination_triggered(bool checked)
{
//    QPoint dstPos = ui->gvDst->mapToGlobal(QPoint(0,0));
//    dstPos = ui->horizontalLayoutWidget_5->mapFromGlobal(dstPos);
    QPoint dstPos;
    dstPos = ui->label8ConnValuesSrc->mapToGlobal(QPoint(0,0));
    dstPos -= ui->gvSrc->mapToGlobal(QPoint(0,0));
    dstPos += ui->gvDst->mapToGlobal(QPoint(0,0));
    dstPos = ui->horizontalLayoutWidget_5->mapFromGlobal(dstPos);

    ui->actionCursor_Destination->setChecked(checked);
    ui->label8ConnValuesDst->move(dstPos);
    ui->label8ConnValuesDst->setVisible(checked);
}

void MainWindow::on_actionCursor_Source_triggered(bool checked)
{
    ui->actionPixel_Cursor_Source->setChecked(checked);
    on_actionPixel_Cursor_Source_triggered(checked);
}

void MainWindow::on_actionCursor_Destination_triggered(bool checked)
{
    ui->actionPixel_Cursor_Destination->setChecked(checked);
    on_actionPixel_Cursor_Destination_triggered(checked);
}

void MainWindow::on_tableViewSrc_cellClicked(int row, int column)
{
    QPoint cell;
    cell.setX(column);
    cell.setY(row);
    updatePixelView(cell);
}

void MainWindow::on_tableViewSrc_cellActivated(int row, int column)
{
    on_tableViewDst_cellClicked(row,column);
}

void MainWindow::on_tableViewDst_cellClicked(int row, int column)
{
    QPoint cell;
    cell.setX(column);
    cell.setY(row);
    updatePixelView(cell);
}

void MainWindow::on_tableViewDst_cellActivated(int row, int column)
{
    on_tableViewDst_cellClicked(row,column);
}

// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
