#include "qgrphvwevents.h"

QGrphVwEvents::QGrphVwEvents(QWidget *parent) :
    QGraphicsView(parent)
{
}

void QGrphVwEvents::mousePressEvent(QMouseEvent *event)
{
//    QPoint pr, pg;

//    pr = event->pos();
//    pg = event->globalPos();
    // STUUR DE pos DOOR DOOR. GEBRUIK 'globalPos()' voor de globale positie
//    emit clicked(event->pos(), event->globalPos());
    emit clicked(event);
}

// -----------------------------------------------

QLabelPassOnEvents::QLabelPassOnEvents(QWidget *parent) :
    QLabel(parent)
{
}

void QLabelPassOnEvents::mousePressEvent(QMouseEvent *event)
{
//    QPoint pr, pg;

//    pr = event->pos();
//    pg = event->globalPos();
    // STUUR DE pos DOOR DOOR. GEBRUIK 'globalPos()' voor de globale positie
//    emit clicked(event->pos(), event->globalPos());
    emit clicked(event);
}
