/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2013 HAN Embedded Systems Engineering
 * Author     : Max van den Beld
 *
 * Description: Source file
 *              By default, events are only picked up by the top most Widget.
 *              This class will pass on the event to the next widget.
 ******************************************************************************
  Change History:

    Version 2.2 - April 2013
    > Initial revision (Thx2 Ken Steenis): Introduced with mainwindow version 2.2

******************************************************************************/
#include "qpassonevents.h"

QGraphicsViewPassOnEvents::QGraphicsViewPassOnEvents(QWidget *parent) :
    QGraphicsView(parent)
{
}

void QGraphicsViewPassOnEvents::mousePressEvent(QMouseEvent *event)
{
//    QPoint pr, pg;

//    pr = event->pos();
//    pg = event->globalPos();
//    STUUR DE pos DOOR DOOR. GEBRUIK 'globalPos()' voor de globale positie
//    emit clicked(event->pos(), event->globalPos());
    emit clicked(event);
}

// -----------------------------------------------

QLabelPassOnEvents::QLabelPassOnEvents(QWidget *parent) :
    QLabel(parent)
{
}

void QLabelPassOnEvents::mousePressEvent(QMouseEvent *event)
{
//    QPoint pr, pg;

//    pr = event->pos();
//    pg = event->globalPos();
//    STUUR DE pos DOOR DOOR. GEBRUIK 'globalPos()' voor de globale positie
//    emit clicked(event->pos(), event->globalPos());
    emit clicked(event);
}
// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
