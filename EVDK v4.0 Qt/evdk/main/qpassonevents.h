/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2013 HAN Embedded Systems Engineering
 * Author     : Max van den Beld
 *
 * Description: Header file
 *              By default, events are only picked up by the top most Widget.
 *              This class will pass on the event to the next widget.
 ******************************************************************************
  Change History:

    Version 2.2 - April 2013
    > Initial revision (Thx2 Ken Steenis): Introduced with mainwindow version 2.2

******************************************************************************/
#ifndef QPASSONEVENTS_H
#define QPASSONEVENTS_H

// General include
#include <QMouseEvent>

// Specific include
#include <QGraphicsView>
#include <QLabel>


class QGraphicsViewPassOnEvents : public QGraphicsView
{
    Q_OBJECT

public:
    explicit QGraphicsViewPassOnEvents(QWidget *parent = 0);

    void mousePressEvent(QMouseEvent *event);

signals:
    void clicked(QMouseEvent *event);

public slots:

};


//----------------------------------------------
class QLabelPassOnEvents : public QLabel
{
    Q_OBJECT

public:
    explicit QLabelPassOnEvents(QWidget *parent = 0);

    void mousePressEvent(QMouseEvent *event);

signals:
    void clicked(QMouseEvent *event);

public slots:

};

#endif // QPASSONEVENTS_H

// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
