#ifndef QGRPHVWEVENTS_H
#define QGRPHVWEVENTS_H

#include <QGraphicsView>
#include <QLabel>
#include <QMouseEvent>


class QGrphVwEvents : public QGraphicsView
{
    Q_OBJECT

public:
    explicit QGrphVwEvents(QWidget *parent = 0);

    void mousePressEvent(QMouseEvent *event);

signals:
    void clicked(QMouseEvent *event);

public slots:

};

//----------------------------------------------

class QLabelPassOnEvents : public QLabel
{
    Q_OBJECT

public:
    explicit QLabelPassOnEvents(QWidget *parent = 0);

    void mousePressEvent(QMouseEvent *event);

signals:
    void clicked(QMouseEvent *event);

public slots:

};

#endif // QGRPHVWEVENTS_H
