/******************************************************************************
 * Project    : Embedded Vision Design
 * Copyright  : 2012 HAN Embedded Systems Engineering
 * Author     : Sander
 *
 * Description: Header file for the main program
 *
 ******************************************************************************
  Change History:

    Version 2.2 - April 2013
    > Added functionality: PixelView || Menubar
    > Redesigned: GUI || About.
    > Fix: Disconnect on CloseApp

    Version 2.1 - November 2012
    > Enabled source image LUT
    > Added save options for source and destination images
    > Fixed memory leak when displaying images

    Version 2.0 - October 2012
    > Merged EVDK hardware + histogram

    Version 1.0 - September 2011
    > Initial revision

******************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QComboBox>
#include <QToolTip>
#include "stdlib.h"
#include "operators.h"
#include "ft245app.h"
#include "aboutevdk.h"

// ----------------------------------------------------------------------------
// Defines
// ----------------------------------------------------------------------------
// Histogram attributes
#define HIST_HEIGHT (200)
#define HIST_WIDTH  (256)

#define EXPAND (300)

// ----------------------------------------------------------------------------
// Type definitions
// ----------------------------------------------------------------------------
typedef enum
{
    RUNMODE_DEFAULT = 0,
    RUNMODE_SNAPSHOT,
    RUNMODE_CONTINUOUS,
    RUNMODE_FILE_TO_DEVBOARD
}runmode_t;

typedef struct
{
    uint16_t width;
    uint16_t height;
    uint16_t min;
    uint16_t max;
    uint16_t high;
    uint16_t mean;
    uint8_t  data[HIST_HEIGHT][HIST_WIDTH];
}draw_hist_t;

namespace Ui
{
    class MainWindow;
}

// ----------------------------------------------------------------------------
// Classes
// ----------------------------------------------------------------------------
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *);

private:
    Ui::MainWindow *ui;
    void updateWindowSize(void);
    void initTableView(void);
    QString buildPixelViewTxt( QPoint msPos, image_t *img );
    void updateTableWidgetSrc(void);
    void updateTableWidgetDst(void);
    void updatePixelView(QPoint e);

    void menuButtonEvdkControlSetVisible(bool yes);

    void DrawImage(QGraphicsView *gview, QGraphicsScene *scene, QComboBox *combobox, image_t *img);
    void DrawHist(QGraphicsView *gview, QGraphicsScene *scene, draw_hist_t *hist);
    void DrawHistogram(image_t *src, draw_hist_t *hist);
    void DrawHistogramInfoSrc(void);
    void DrawHistogramInfoDst(void);
    void saveJL(image_t *img, QString filename);

    aboutEvdk evdkAbout;

    runmode_t runmode;
    ft245app *devKit;

    QStringList LUTlist;
    QStringList VSlist;

    image_t imgSrc;
    image_t imgDst;
    image_t imgDstDisplayed;

    draw_hist_t histSrc;
    draw_hist_t histDst;

    QGraphicsScene sceneSrcImg;
    QGraphicsScene sceneDstImg;
    QGraphicsScene sceneSrcHist;
    QGraphicsScene sceneDstHist;

    bool ContinuousRunning;
    void setMode(runmode_t m);

public slots:
    void aboutClicked();

    void exitClickedHandler();
    void openImageFile();
    void executeVisionSet();

    void LUTSrcChanged();
    void LUTDstChanged();

    void saveSrcBmpClicked();
    void saveDstBmpClicked();
    void saveSrcJlClicked();
    void saveDstJlClicked();

    void destroyedemitted();
    void vShowDstImage();

    void onHistogram_clicked(QMouseEvent *event);
    void onPixelView_clicked(QMouseEvent *event );
    void onlabel8ConnValuesSrc_clicked(QMouseEvent *);
    void onlabel8ConnValuesDst_clicked(QMouseEvent *);

    void NewImage(int, image_t);
    void NewInfo(QString);

private slots:
    void on_actionShow_histogram_src_triggered(bool checked);
    void on_actionShow_histogram_dst_triggered(bool checked);

    void on_actionOpen_triggered();
    void on_actionClose_triggered();
    void on_actionSnapshot_triggered();
    void on_actionFile_to_DevBoard_triggered();
    void on_actionContinuous_triggered();

    void on_Operator_currentIndexChanged(int index);
    void on_spinBoxDevKit_valueChanged(int arg1);

    void on_pushButtonSingle_clicked();
    void on_actionPixel_Table_Source_triggered(bool checked);
    void on_actionPixel_Table_Destination_triggered(bool checked);

    void on_actionTable_Source_triggered(bool checked);
    void on_actionTable_Destination_triggered(bool checked);

    void on_actionCursor_Source_triggered(bool checked);
    void on_actionCursor_Destination_triggered(bool checked);

    void on_actionPixel_Cursor_Source_triggered(bool checked);
    void on_actionPixel_Cursor_Destination_triggered(bool checked);

    void on_actionBbSnapshot_triggered();
    void on_actionBbFile_triggered();
    void on_actionBbContinuous_triggered();

    void on_tableViewSrc_cellClicked(int row, int column);
    void on_tableViewDst_cellClicked(int row, int column);

    void on_tableViewSrc_cellActivated(int row, int column);
    void on_tableViewDst_cellActivated(int row, int column);

    void on_actionBbExit_triggered();
    void on_actionBbHistSrc_triggered();
    void on_actionBbHistDst_triggered();

};

#endif // MAINWINDOW_H
// ----------------------------------------------------------------------------
// EOF
// ----------------------------------------------------------------------------
